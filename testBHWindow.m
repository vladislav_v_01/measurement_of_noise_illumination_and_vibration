BHWindow = BlacmanHarrisWindow(200);
[RealPartBHWindow, ImaginaryPartBHFilter] = colerationDPF(BHWindow);
Amplitude = [];
Numb_Sign = length(RealPartBHWindow);
for i = 1:Numb_Sign 
  AmplitudeValue = sqrt(RealPartBHWindow(i) * RealPartBHWindow(i) + ImaginaryPartBHFilter(i) * ImaginaryPartBHFilter(i));
  Amplitude = [Amplitude AmplitudeValue]  
endfor
Numb_Amplitude = length(Amplitude);
AmplitudeIndB = [];
for i = 1:Numb_Amplitude 
  value = 20 * log10(Amplitude(i)/71.39131);
  AmplitudeIndB = [AmplitudeIndB value]  
endfor

figure(1);
##subplot(5,1,1);
##plot(BHWindow);
##subplot(5,1,2);
##plot(RealPartBHWindow);
##subplot(5,1,3);
##plot(ImaginaryPartBHFilter);
##subplot(5,1,4);
##plot(Amplitude);
##subplot(5,1,5);
plot(AmplitudeIndB);