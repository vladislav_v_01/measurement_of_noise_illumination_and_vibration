#include "staticaldataprocessor.h"

StaticalDataProcessor::StaticalDataProcessor(QObject* p_parent)
    : QObject(p_parent)
{
}

StaticalDataProcessor::~StaticalDataProcessor() {}

/*
void StaticalDataProcessor::fourierTransform(const
std::vector<StaticalDataProcessor::T> &signal, std::vector<double>&
resultSignal)
{
    int     i, j, n, m, Mmax, Istp;
    double  Tmpr, Tmpi, Wtmp, Theta;
    double  Wpr, Wpi, Wr, Wi;
    double* Tmvl;
    int     Nvl = signal.size();
    int     Nft = signal.size() / 2.0;

    n    = Nvl * 2;
    Tmvl = new double[n];

    for (i = 0; i < n; i += 2)
    {
        Tmvl[i]     = 0;
        Tmvl[i + 1] = signal[i / 2];
    }

    i = 1;
    j = 1;
    while (i < n)
    {
        if (j > i)
        {
            Tmpr        = Tmvl[i];
            Tmvl[i]     = Tmvl[j];
            Tmvl[j]     = Tmpr;
            Tmpr        = Tmvl[i + 1];
            Tmvl[i + 1] = Tmvl[j + 1];
            Tmvl[j + 1] = Tmpr;
        }
        i = i + 2;
        m = Nvl;
        while ((m >= 2) && (j > m))
        {
            j = j - m;
            m = m >> 1;
        }
        j = j + m;
    }

    Mmax = 2;
    while (n > Mmax)
    {
        Theta = -TwoPi / Mmax;
        Wpi   = sin(Theta);
        Wtmp  = sin(Theta / 2);
        Wpr   = Wtmp * Wtmp * 2;
        Istp  = Mmax * 2;
        Wr    = 1;
        Wi    = 0;
        m     = 1;

        while (m < Mmax)
        {
            i    = m;
            m    = m + 2;
            Tmpr = Wr;
            Tmpi = Wi;
            Wr   = Wr - Tmpr * Wpr - Tmpi * Wpi;
            Wi   = Wi + Tmpr * Wpi - Tmpi * Wpr;

            while (i < n)
            {
                j    = i + Mmax;
                Tmpr = Wr * Tmvl[j] - Wi * Tmvl[j - 1];
                Tmpi = Wi * Tmvl[j] + Wr * Tmvl[j - 1];

                Tmvl[j]     = Tmvl[i] - Tmpr;
                Tmvl[j - 1] = Tmvl[i - 1] - Tmpi;
                Tmvl[i]     = Tmvl[i] + Tmpr;
                Tmvl[i - 1] = Tmvl[i - 1] + Tmpi;
                i           = i + Istp;
            }
        }

        Mmax = Istp;
    }

    for (i = 0; i < Nft; i++)
    {
        j = i * 2;
        resultSignal[i] =
            2 * std::sqrt(Tmvl[j] * Tmvl[j] + Tmvl[j + 1] * Tmvl[j + 1]) / Nvl;
    }

    delete[] Tmvl;
}
*/
void StaticalDataProcessor::buildGraph(
    const std::vector<double>& realPartOfSignalInFreaquencePart,
    const std::vector<double>& imaginePartOfSignalInFreaquncePart,
    std::vector<double>&       xAxis,
    std::vector<double>&       yAxis)
{
    // формирование данных по осям координат
    // формирование данных по оси y
    const int yAxisPointCount = realPartOfSignalInFreaquencePart.size();
    for (int i = 0; i < yAxisPointCount; i++)
    {
        const double realValue    = realPartOfSignalInFreaquencePart[i];
        const double imagineValue = imaginePartOfSignalInFreaquncePart[i];
        const double module = complexNumberModulus(realValue, imagineValue);

        yAxis.push_back(module);
        // xAxisPointCount = SampleRate / 2.0;
        // xAxis.push_back(i / 2.0);
    }
    // формирование данных по оси х
    // по оси х данные должны быть не больше SampleRate / 2.0
    // 1) береться частота дискретизации / 2.0
    // 2) береться время записи сигнала (Т)
    // 3) 1/Т = шаг  по оси х в Гц
    // 4) заполняем ось х с шагом 1/Т до SampleRate / 2.0
    // ------------------------------------------------------------------------
    // для отладки значение времени записи возьмём в 2000 мс
    // SampleRate = 48000
    // следовательно шаг по оси х равен 0.5 Гц
    for (int i = 0; i < 32768; i++)
    {
        xAxis.push_back(i * 0.5);
    }
}

double StaticalDataProcessor::complexNumberModulus(const double realValue,
                                                   const double imagineValue)
{
    return std::sqrt(realValue * realValue + imagineValue * imagineValue);
}
