#include "noisemeterworker.h"

#include <chrono>
#include <thread>

#include <QDebug>
NoiseMeterWorker::NoiseMeterWorker(QObject* p_parent)
    : QObject(p_parent)
    , m_noiseChartView(nullptr)
    , m_minValueLabel(nullptr)
    , m_avgValueLabel(nullptr)
    , m_maxValueLabel(nullptr)
    , m_angle(-44)
{
    this->initializeAudio(QAudioDeviceInfo::defaultInputDevice());
}
NoiseMeterWorker::~NoiseMeterWorker()
{
    this->m_audioInput->stop();
    this->m_audioInfo->close();
    delete m_audioInfo;
    delete m_audioInput;
}
void NoiseMeterWorker::onItemCompleted(QQuickItem* newItem)
{
    Q_ASSERT(newItem != nullptr);
    const auto& itemName = newItem->objectName();
    if (itemName == "minValueLabel")
    {
        this->m_minValueLabel = newItem;
    }
    else if (itemName == "avgValueLabel")
    {
        this->m_avgValueLabel = newItem;
    }
    else if (itemName == "maxValueLabel")
    {
        this->m_maxValueLabel = newItem;
    }
    else if (itemName == "vibrometerArrowImage")
    {
        this->m_vibrometerArrowImage = newItem;
    }
}
void NoiseMeterWorker::onItemDestroyed(QQuickItem* newItem)
{
    Q_ASSERT(newItem != nullptr);
    const auto& itemName = newItem->objectName();
    if (itemName == "minValueLabel")
    {
        this->m_minValueLabel = nullptr;
    }
    else if (itemName == "avgValueLabel")
    {
        this->m_avgValueLabel     = nullptr;
        this->m_summOfMeasurings  = 0.0;
        this->m_countOfMeasurings = 0;
    }
    else if (itemName == "maxValueLabel")
    {
        this->m_maxValueLabel = nullptr;
    }
    else if (itemName == "noiseMeterPlot")
    {
        this->m_audioInput->stop();
        this->m_audioInfo->close();
    }
    else if (itemName == "vibrometerArrowImage")
    {
        this->m_vibrometerArrowImage = nullptr;
    }
}
void NoiseMeterWorker::onNewSeries()
{
    this->m_audioInfo = new NoiseMeter(this);
    QObject::connect(m_audioInfo,
                     &NoiseMeter::newDataWrited,
                     this,
                     &NoiseMeterWorker::onNewDataWrited);
    this->m_audioInfo->open(QIODevice::WriteOnly);
    this->m_audioInput->start(this->m_audioInfo);
}
void NoiseMeterWorker::onNewDataWrited(QVector<QPointF> data)
{
    ((QtCharts::QLineSeries*)m_series)->replace(data);
    QVector<QPointF> lastAddedData(63, QPointF{ 0.0f, 0.0f });
    std::copy(data.end() - 63, data.end(), lastAddedData.begin());
    std::sort(lastAddedData.begin(),
              lastAddedData.end(),
              [](const QPointF& p1, const QPointF& p2)
              { return p1.y() < p2.y(); });
    //    double currValue = lastAddedData.back().y();
    if (this->m_avgValueLabel != nullptr)
    {
        this->m_countOfMeasurings += 63;
        m_summOfMeasurings =
            std::accumulate(lastAddedData.begin(),
                            lastAddedData.end(),
                            0.0,
                            [](double summOfMeasurings, const QPointF& point)
                            { return summOfMeasurings += point.y(); });
        //        qDebug() << m_summOfMeasurings;
        double avgValue = this->m_summOfMeasurings;
        //        qDebug() << avgValue;
        QString avgValueText =
            QString::number(int64_t(100 * (avgValue + 1) / 2.0));
        //        qDebug() << avgValueText;
        int diffForAvgValue =
            m_countOFSymbolsOnValueLabels - avgValueText.count();
        this->m_avgValueLabel->setProperty(
            "text", QString(diffForAvgValue, '0') + avgValueText);
    }
    if (this->m_minValueLabel != nullptr)
    {
        const auto minY =
            std::min_element(lastAddedData.begin(),
                             lastAddedData.end(),
                             [](const QPointF& p1, const QPointF& p2)
                             { return p1.y() < p2.y(); })
                ->y();
        QString newValueText =
            QString::number(int64_t(50 * ((minY + 1) / 2.0)));
        int     diff = m_countOFSymbolsOnValueLabels - newValueText.count();
        QString currMinValue =
            this->m_minValueLabel->property("text").toString();
        QString newValue = QString(diff, '0') + newValueText;
        this->m_minValueLabel->setProperty(
            "text",
            qFuzzyIsNull(currMinValue.toDouble())
                ? newValue
                : (currMinValue.toDouble() < newValue.toDouble() ? currMinValue
                                                                 : newValue));
    }
    if (this->m_maxValueLabel != nullptr)
    {
        const auto maxY =
            std::max_element(lastAddedData.begin(),
                             lastAddedData.end(),
                             [](const QPointF& p1, const QPointF& p2)
                             { return p1.y() < p2.y(); })
                ->y();
        QString newValueText =
            QString::number(int64_t(100 * ((maxY + 1) / 2.0) - 30));
        int     diff = m_countOFSymbolsOnValueLabels - newValueText.count();
        QString currMaxValue =
            this->m_maxValueLabel->property("text").toString();
        QString newValue = QString(diff, '0') + newValueText;
        this->m_maxValueLabel->setProperty(
            "text",
            currMaxValue.toDouble() > newValue.toDouble() ? currMaxValue
                                                          : newValue);
    }
    if (this->m_vibrometerArrowImage != nullptr)
    {
        //        const auto angle = 75 * currValue /*+ 1) / 2.0)*/ * 270.0 /
        //        120.0; this->m_vibrometerArrowImage->setProperty("angle",
        //        angle);
    }
}
void NoiseMeterWorker::initializeAudio(const QAudioDeviceInfo& deviceInfo)
{
    QAudioFormat format;
    format.setSampleRate(8000);
    format.setChannelCount(1);
    format.setSampleSize(8);
    format.setCodec("audio/pcm");
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(QAudioFormat::UnSignedInt);
    if (!deviceInfo.isFormatSupported(format))
    {
        qWarning() << "Default format not supported";
        std::this_thread::sleep_for(std::chrono::milliseconds(10000));
    }
    this->m_audioInput = new QAudioInput(deviceInfo, format);
}
