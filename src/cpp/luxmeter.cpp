#include "luxmeter.h"

#include <iostream>

LuxMeter::LuxMeter(QObject* parent)
    : QObject(parent)
    , m_lightSensor(new QLightSensor(this))
    , m_lux(0)
{
    connect(this->m_lightSensor,
            &QLightSensor::readingChanged,
            [&]()
            {
                this->m_lux = this->m_lightSensor->reading()->lux();
                emit this->newValue(this->m_lux);
            });
}

LuxMeter::~LuxMeter() {}

qreal LuxMeter::getLux() const
{
    return this->m_lux;
}

void LuxMeter::startListening()
{
    this->m_lightSensor->setDataRate(0);
    this->m_lightSensor->start();
}

void LuxMeter::stopListening()
{
    this->m_lightSensor->stop();
}
