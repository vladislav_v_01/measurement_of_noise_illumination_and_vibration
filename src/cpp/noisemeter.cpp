#include "noisemeter.h"

#include <QDebug>
#include <QtEndian>
NoiseMeter::NoiseMeter(QObject* p_parent)
    : QIODevice(p_parent)
{
}
NoiseMeter::~NoiseMeter()
{
    if (this->isOpen())
    {
        this->close();
    }
}
qint64 NoiseMeter::readData(char* data, qint64 maxlen)
{
    Q_UNUSED(data);
    Q_UNUSED(maxlen);
    return -1;
}
qint64 NoiseMeter::writeData(const char* data, qint64 maxSize)
{
    static const int resolution = 4;

    if (m_buffer.isEmpty())
    {
        m_buffer.reserve(sampleCount);
        for (int i = 0; i < sampleCount; ++i)
            m_buffer.append(QPointF(i, 0));
    }

    int       start            = 0;
    const int availableSamples = int(maxSize) / resolution;

    if (availableSamples < sampleCount)
    {
        start = sampleCount - availableSamples;
        for (int s = 0; s < start; ++s)
            m_buffer[s].setY(m_buffer.at(s + availableSamples).y());
    }

    for (int s = start; s < sampleCount; ++s, data += resolution)
        m_buffer[s].setY(qreal(uchar(*data) - 128) / qreal(128));

    emit newDataWrited(m_buffer);

    return (sampleCount - start) * resolution;
}
