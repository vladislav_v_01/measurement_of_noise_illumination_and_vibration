#include "qcustomplotqml.h"
#include "qcustomplot.h"
#include <QDebug>

CustomPlotItem::CustomPlotItem(QQuickItem* parent)
    : QQuickPaintedItem(parent)
    , m_CustomPlot(nullptr)
    , m_timerId(0)
{
    setFlag(QQuickItem::ItemHasContents, true);
    setAcceptedMouseButtons(Qt::AllButtons);

    connect(this,
            &QQuickPaintedItem::widthChanged,
            this,
            &CustomPlotItem::updateCustomPlotSize);
    connect(this,
            &QQuickPaintedItem::heightChanged,
            this,
            &CustomPlotItem::updateCustomPlotSize);
}

CustomPlotItem::~CustomPlotItem()
{
    delete m_CustomPlot;
    m_CustomPlot = nullptr;

    if (m_timerId != 0)
    {
        killTimer(m_timerId);
    }
}

void CustomPlotItem::initCustomPlot()
{
    m_CustomPlot = new QCustomPlot();

    updateCustomPlotSize();
    m_CustomPlot->addGraph();
    m_CustomPlot->graph(0)->setPen(QPen(Qt::red, 5.0));
    m_CustomPlot->setBackground(QBrush(QColor("#29353B")));
    //    m_CustomPlot->xAxis->setLabel( "t" );
    //    m_CustomPlot->yAxis->setLabel( "S" );
    m_CustomPlot->xAxis->setBasePen(QPen(Qt::white, 2.0));
    m_CustomPlot->xAxis->setTickPen(QPen(Qt::white, 2.0));
    m_CustomPlot->xAxis->grid()->setVisible(true);
    m_CustomPlot->xAxis->grid()->setPen(
        QPen(QColor(200, 200, 200), 2.0, Qt::DotLine));
    m_CustomPlot->xAxis->setTickLabelColor(Qt::white);
    m_CustomPlot->xAxis->setLabelColor(Qt::white);

    m_CustomPlot->yAxis->setBasePen(QPen(Qt::white, 2.0));
    m_CustomPlot->yAxis->setTickPen(QPen(Qt::white, 2.0));
    m_CustomPlot->yAxis->setSubTickPen(QPen(Qt::white, 2.0));
    m_CustomPlot->yAxis->grid()->setSubGridVisible(true);
    m_CustomPlot->yAxis->setTickLabelColor(Qt::white);
    m_CustomPlot->yAxis->setLabelColor(Qt::white);
    m_CustomPlot->yAxis->grid()->setPen(
        QPen(QColor(200, 200, 200), 2.0, Qt::SolidLine));
    m_CustomPlot->yAxis->grid()->setSubGridPen(
        QPen(QColor(130, 130, 130), 2.0, Qt::DotLine));

    m_CustomPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    /*  startTimer(500);

    connect( m_CustomPlot, &QCustomPlot::afterReplot, this,
    &CustomPlotItem::onCustomReplot );*/

    m_CustomPlot->replot();
}

void CustomPlotItem::paint(QPainter* painter)
{
    if (m_CustomPlot)
    {
        QPixmap    picture(boundingRect().size().toSize());
        QCPPainter qcpPainter(&picture);

        m_CustomPlot->toPainter(&qcpPainter);

        painter->drawPixmap(QPoint(), picture);
    }
}

void CustomPlotItem::mousePressEvent(QMouseEvent* event)
{
    // qDebug() << Q_FUNC_INFO;
    routeMouseEvents(event);
}

void CustomPlotItem::mouseReleaseEvent(QMouseEvent* event)
{
    // qDebug() << Q_FUNC_INFO;
    routeMouseEvents(event);
}

void CustomPlotItem::mouseMoveEvent(QMouseEvent* event)
{
    routeMouseEvents(event);
}

void CustomPlotItem::mouseDoubleClickEvent(QMouseEvent* event)
{
    //  qDebug() << Q_FUNC_INFO;
    routeMouseEvents(event);
}

void CustomPlotItem::wheelEvent(QWheelEvent* event)
{
    routeWheelEvents(event);
}

void CustomPlotItem::timerEvent(QTimerEvent* event)
{
    static double t, U;
    U = ((double)rand() / RAND_MAX) * 5;
    m_CustomPlot->graph(0)->addData(t, U);
    //  qDebug() << Q_FUNC_INFO << QString("Adding dot t = %1, S =
    //  %2").arg(t).arg(U);
    t++;
    m_CustomPlot->replot();
}

void CustomPlotItem::graphClicked(QCPAbstractPlottable* plottable)
{
    //  qDebug() << Q_FUNC_INFO << QString( "Clicked on graph '%1 " ).arg(
    //  plottable->name() );
}

void CustomPlotItem::routeMouseEvents(QMouseEvent* event)
{
    if (m_CustomPlot)
    {
        QMouseEvent* newEvent = new QMouseEvent(event->type(),
                                                event->localPos(),
                                                event->button(),
                                                event->buttons(),
                                                event->modifiers());
        QCoreApplication::postEvent(m_CustomPlot, newEvent);
    }
}

void CustomPlotItem::routeWheelEvents(QWheelEvent* event)
{
    if (m_CustomPlot)
    {
        QWheelEvent* newEvent = new QWheelEvent(event->pos(),
                                                event->delta(),
                                                event->buttons(),
                                                event->modifiers(),
                                                event->orientation());
        QCoreApplication::postEvent(m_CustomPlot, newEvent);
    }
}

void CustomPlotItem::updateCustomPlotSize()
{
    if (m_CustomPlot)
    {
        m_CustomPlot->setGeometry(0, 0, (int)width(), (int)height());
        m_CustomPlot->setViewport(QRect(0, 0, (int)width(), (int)height()));
    }
}

void CustomPlotItem::onCustomReplot()
{
    //  qDebug() << Q_FUNC_INFO;
    update();
}
