#include "appsettingsdialog.h"
#include "../ui/ui_appsettingsdialog.h"

#include "appsettings.h"
#include "singleton.h"

#include <iostream>

#include <QRegExpValidator>

AppSettingsDialog::AppSettingsDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::AppSettingsDialog)
{
    ui->setupUi(this);

    const int sampleRate = SINGLETON(AppSettings)->get_sampleRate();
    ui->samplingRateComboBox->setCurrentIndex(
        ui->samplingRateComboBox->findText(QString::number(sampleRate) +
                                           tr(" Гц")));

    const int channelCount = SINGLETON(AppSettings)->get_channelCount();
    ui->channelCountComboBox->setCurrentIndex(
        ui->channelCountComboBox->findText(QString::number(channelCount)));

    const int sampleSize = SINGLETON(AppSettings)->get_sampleSize();
    QString   bins       = sampleSize == 32 ? tr(" бита") : tr(" бит");
    ui->sampleSizeComboBox->setCurrentIndex(
        ui->sampleSizeComboBox->findText(QString::number(sampleSize) + bins));

    const int sampleType = SINGLETON(AppSettings)->get_sampleType();
    QString   sampleTypeStr;
    switch (sampleType)
    {
        case 0:
            sampleTypeStr = tr("unknown");
            break;
        case 1:
            sampleTypeStr = tr("signed");
            break;
        case 2:
            sampleTypeStr = tr("unsigned");
            break;
        case 3:
            sampleTypeStr = tr("float");
            break;
        default:
            std::cerr << Q_FUNC_INFO << __LINE__
                      << tr(" Ошибка, сюда невозможно попасть!!!").toStdString()
                      << std::endl;
            sampleTypeStr = tr("unknown");
            break;
    }
    ui->sampleTypeComboBox->setCurrentIndex(
        ui->sampleTypeComboBox->findText(sampleTypeStr));

    const int bufferSize = SINGLETON(AppSettings)->get_bufferSize();
    bins =
        (bufferSize == 1024 || bufferSize == 8192) ? tr(" бита") : tr(" бит");
    ui->bufferSizeComboBox->setCurrentIndex(
        ui->bufferSizeComboBox->findText(QString::number(bufferSize) + bins));
}

AppSettingsDialog::~AppSettingsDialog()
{
    delete ui;
}

void AppSettingsDialog::on_buttonBox_accepted()
{
    QString sampleRateStr = ui->samplingRateComboBox->currentText();
    sampleRateStr.remove(tr("Гц"));
    const int sampleRate = sampleRateStr.toInt();
    SINGLETON(AppSettings)->set_sampleRate(sampleRate);

    QString   channelCountStr = ui->channelCountComboBox->currentText();
    const int channelCount    = channelCountStr.toInt();
    SINGLETON(AppSettings)->set_channelCount(channelCount);

    QString sampleSizeStr = ui->sampleSizeComboBox->currentText();
    sampleSizeStr.remove(tr("бита"));
    sampleSizeStr.remove(tr("бит"));
    const int sampleSize = sampleSizeStr.toInt();
    SINGLETON(AppSettings)->set_sampleSize(sampleSize);

    QString sampleTypeStr = ui->sampleTypeComboBox->currentText();
    if (sampleTypeStr == "signed")
    {
        SINGLETON(AppSettings)->set_sampleType(QAudioFormat::SignedInt);
    }
    else if (sampleTypeStr == "unsigned")
    {
        SINGLETON(AppSettings)->set_sampleType(QAudioFormat::UnSignedInt);
    }
    else if (sampleTypeStr == "float")
    {
        SINGLETON(AppSettings)->set_sampleType(QAudioFormat::Float);
    }
    else
    {
        SINGLETON(AppSettings)->set_sampleType(QAudioFormat::Unknown);
    }

    QString bufferSizeStr = ui->bufferSizeComboBox->currentText();
    bufferSizeStr.remove(tr("бита"));
    bufferSizeStr.remove(tr("бит"));
    const int bufferSize = bufferSizeStr.toInt();
    SINGLETON(AppSettings)->set_bufferSize(bufferSize);

    close();
}

void AppSettingsDialog::on_buttonBox_rejected()
{
    close();
}
