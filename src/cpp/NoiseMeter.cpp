//#include "NoiseMeter.hpp"

//#include "libs/QRealFourier/headers/qfouriertransformer.h"

// const int SIZE = 1024;

// AudioInfo::AudioInfo(const QAudioFormat& format)
//    : m_format(format)
//{
//    switch (m_format.sampleSize())
//    {
//        case 8:
//            switch (m_format.sampleType())
//            {
//                case QAudioFormat::UnSignedInt:
//                    m_maxAmplitude = 255;
//                    break;
//                case QAudioFormat::SignedInt:
//                    m_maxAmplitude = 127;
//                    break;
//                default:
//                    break;
//            }
//            break;
//        case 16:
//            switch (m_format.sampleType())
//            {
//                case QAudioFormat::UnSignedInt:
//                    m_maxAmplitude = 65535;
//                    break;
//                case QAudioFormat::SignedInt:
//                    m_maxAmplitude = 32767;
//                    break;
//                default:
//                    break;
//            }
//            break;

//        case 32:
//            switch (m_format.sampleType())
//            {
//                case QAudioFormat::UnSignedInt:
//                    m_maxAmplitude = 0xffffffff;
//                    break;
//                case QAudioFormat::SignedInt:
//                    m_maxAmplitude = 0x7fffffff;
//                    break;
//                case QAudioFormat::Float:
//                    m_maxAmplitude = 0x7fffffff; // Kind of
//                default:
//                    break;
//            }
//            break;

//        default:
//            break;
//    }
//}

// void AudioInfo::start()
//{
//    open(QIODevice::WriteOnly);
//}

// void AudioInfo::stop()
//{
//    close();
//}

// qint64 AudioInfo::readData(char* data, qint64 maxlen)
//{
//    Q_UNUSED(data)
//    Q_UNUSED(maxlen)

//    return 0;
//}

// qint64 AudioInfo::writeData(const char* data, qint64 len)
//{
//    if (m_maxAmplitude)
//    {
//        Q_ASSERT(m_format.sampleSize() % 8 == 0);
//        const int channelBytes = m_format.sampleSize() / 8;
//        const int sampleBytes  = m_format.channelCount() * channelBytes;
//        Q_ASSERT(len % sampleBytes == 0);
//        const int numSamples = len / sampleBytes;

//        quint32              maxValue = 0;
//        const unsigned char* ptr = reinterpret_cast<const unsigned
//        char*>(data); m_sampledBufferF.clear();

//        for (int i = 0; i < numSamples; ++i)
//        {
//            for (int j = 0; j < m_format.channelCount(); ++j)
//            {
//                quint32 value   = 0;
//                float   value_f = 0.0;

//                if (m_format.sampleSize() == 8 &&
//                    m_format.sampleType() == QAudioFormat::UnSignedInt)
//                {
//                    value = *reinterpret_cast<const quint8*>(ptr);
//                }
//                else if (m_format.sampleSize() == 8 &&
//                         m_format.sampleType() == QAudioFormat::SignedInt)
//                {
//                    value = qAbs(*reinterpret_cast<const qint8*>(ptr));
//                }
//                else if (m_format.sampleSize() == 16 &&
//                         m_format.sampleType() == QAudioFormat::UnSignedInt)
//                {
//                    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
//                        value = qFromLittleEndian<quint16>(ptr);
//                    else
//                        value = qFromBigEndian<quint16>(ptr);
//                }
//                else if (m_format.sampleSize() == 16 &&
//                         m_format.sampleType() == QAudioFormat::SignedInt)
//                {
//                    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
//                        value = qAbs(qFromLittleEndian<qint16>(ptr));
//                    else
//                        value = qAbs(qFromBigEndian<qint16>(ptr));
//                }
//                else if (m_format.sampleSize() == 32 &&
//                         m_format.sampleType() == QAudioFormat::UnSignedInt)
//                {
//                    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
//                        value = qFromLittleEndian<quint32>(ptr);
//                    else
//                        value = qFromBigEndian<quint32>(ptr);
//                }
//                else if (m_format.sampleSize() == 32 &&
//                         m_format.sampleType() == QAudioFormat::SignedInt)
//                {
//                    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
//                        value = qAbs(qFromLittleEndian<qint32>(ptr));
//                    else
//                        value = qAbs(qFromBigEndian<qint32>(ptr));
//                }
//                else if (m_format.sampleSize() == 32 &&
//                         m_format.sampleType() == QAudioFormat::Float)
//                {
//                    value   = qAbs(*reinterpret_cast<const float*>(ptr) *
//                                 0x7fffffff); // assumes 0-1.0
//                    value_f = (*reinterpret_cast<const float*>(ptr));
//                }
//                if (m_sampledAudioF.size() < 98304 && !mesauringFinished)
//                {

//                    qreal attitudeCurrValueToMax =
//                        value / std::pow(2, m_format.sampleSize());
//                    //                    qreal valueIndB = 20 *
//                    //                    log10(attitudeCurrValueToMax);
//                    //                    qreal valueIndB =
//                    //                        QAudio::convertVolume(value /
//                    //                        qreal(100),
//                    // QAudio::LogarithmicVolumeScale,
//                    // QAudio::DecibelVolumeScale);
//                    // m_sampledAudioInbB.push_back(valueIndB);
//                    //                    m_sampledAudio.push_back(value);
//                    m_sampledAudioF.push_back(float(value));
//                    //                m_sampledAudio.push_back(value);
//                }
//                else if (m_sampledAudioF.size() < 131072 &&
//                !mesauringFinished)
//                {
//                    for (int i = 0; i < 32768; i++)
//                    {
//                        m_sampledAudio.push_back(0);
//                        // m_sampledAudioInbB.push_back(0);
//                    }
//                    emit audioSampled();
//                    mesauringFinished = true;
//                }
//                else
//                {
//                    // TO DO
//                }
//                m_sampledBufferF.push_back(value_f);

//                maxValue = qMax(value, maxValue);
//                ptr += channelBytes;
//            }
//        }

//#ifdef NEED_DEBUG_INFO
//        auto maxAudioLevel =
//            std::max_element(m_sampledAudio.begin(), m_sampledAudio.end());

//        m_maxAudioLevel = *maxAudioLevel;
//#endif
//        maxValue = qMin(maxValue, m_maxAmplitude);
//        m_level  = qreal(maxValue) / m_maxAmplitude;
//    }

//    emit update();
//    emit bufferSampled();
//    return len;
//}

// std::vector<quint32> AudioInfo::getSampledAudio()
//{
//    return m_sampledAudio;
//}

// std::vector<qreal> AudioInfo::getSampledAudioIndB()
//{
//    return m_sampledAudioInbB;
//}

// std::vector<float> AudioInfo::getSampledAudioF()
//{
//    return m_sampledAudioF;
//}

// std::vector<float> AudioInfo::getSampledBufferF()
//{
//    return m_sampledBufferF;
//}

// RenderArea::RenderArea(QWidget* parent)
//    : QWidget(parent)
//{
//    setBackgroundRole(QPalette::Base);
//    setAutoFillBackground(true);

//    setMinimumHeight(30);
//    setMinimumWidth(200);
//}

// void RenderArea::paintEvent(QPaintEvent* /* event */)
//{
//    QPainter painter(this);

//    painter.setPen(Qt::black);
//    painter.drawRect(QRect(painter.viewport().left() + 10,
//                           painter.viewport().top() + 10,
//                           painter.viewport().right() - 20,
//                           painter.viewport().bottom() - 20));
//    if (m_level == 0.0)
//        return;

//    int pos =
//        ((painter.viewport().right() - 20) - (painter.viewport().left() + 11))
//        * m_level;
//    painter.fillRect(painter.viewport().left() + 11,
//                     painter.viewport().top() + 10,
//                     pos,
//                     painter.viewport().height() - 21,
//                     Qt::red);
//}

// void RenderArea::setLevel(qreal value)
//{
//    m_level = value;
//    update();
//}

// NoiseMeter::NoiseMeter()
//{
//    initializeWindow();
//    initializeAudio(QAudioDeviceInfo::defaultInputDevice());
//    BHWindow.resize(48000);
//    signalOfLastSecond.resize(48000);
//    buildBlackmanHarrisWindow(BHWindow);
//    LPFilt = buildCoreLPFilt(0.2, 0.02);
//}

// NoiseMeter::~NoiseMeter() {}

// void NoiseMeter::initializeWindow()
//{
//    QWidget*     window = new QWidget;
//    QVBoxLayout* layout = new QVBoxLayout;

//    freaqunceCustomPlot = new QCustomPlot(this);
//    freaqunceCustomPlot->plotLayout()->clear();
//    //    customPlot->setGeometry(400, 250, 500, 600);
//    freaqunceCustomPlot->setSizePolicy(QSizePolicy::Preferred,
//                                       QSizePolicy::Preferred);
//    QCPAxisRect* wideAxisRect = new QCPAxisRect(freaqunceCustomPlot);
//    wideAxisRect->axis(QCPAxis::atBottom)->setScaleType(QCPAxis::stLogarithmic);
//    QCPLayoutGrid* subLayout = new QCPLayoutGrid;
//    freaqunceCustomPlot->plotLayout()->addElement(
//        1, 0, subLayout); // sub layout in second row (grid layout will grow
//    // accordingly)
//    freaqunceCustomPlot->plotLayout()->addElement(
//        0, 0, wideAxisRect); // insert axis rect in first row
//    QCPMarginGroup* marginGroup = new QCPMarginGroup(freaqunceCustomPlot);
//    wideAxisRect->setMarginGroup(QCP::msLeft | QCP::msRight, marginGroup);
//    QCPAxisRect* subRectLeft = new QCPAxisRect(
//        freaqunceCustomPlot, false); // false means to not setup default axes
//    subLayout->addElement(0, 0, subRectLeft);
//    //  setup axes in sub layout axis rects
//    //      :
//    subRectLeft->addAxes(QCPAxis::atBottom | QCPAxis::atLeft);
//    subRectLeft->axis(QCPAxis::atBottom)->setScaleType(QCPAxis::stLogarithmic);
//    subRectLeft->axis(QCPAxis::atBottom)
//        ->setTicker(QSharedPointer<QCPAxisTickerLog>(new QCPAxisTickerLog));
//    //    ->axis->setTicker(QSharedPointer<QCPAxisTickerLog>(new
//    //    QCPAxisTickerLog));
//    //    subRectLeft->axis(QCPAxis::atLeft)->ticker()->setTickCount(2);
//    subRectLeft->axis(QCPAxis::atBottom)->grid()->setVisible(true);
//    subRectLeft->axis(QCPAxis::atLeft)->grid()->setVisible(true);
//    QCPAxisRect* timeDomain = new QCPAxisRect(freaqunceCustomPlot, false);
//    subLayout->addElement(0, 1, timeDomain);
//    timeDomain->addAxes(QCPAxis::atBottom | QCPAxis::atLeft);
//    timeDomain->axis(QCPAxis::atBottom)
//        ->setTicker(QSharedPointer<QCPAxisTickerLog>(new QCPAxisTickerLog));
//    //    ->axis->setTicker(QSharedPointer<QCPAxisTickerLog>(new
//    //    QCPAxisTickerLog));
//    //    subRectLeft->axis(QCPAxis::atLeft)->ticker()->setTickCount(2);
//    timeDomain->axis(QCPAxis::atBottom)->grid()->setVisible(true);
//    timeDomain->axis(QCPAxis::atLeft)->grid()->setVisible(true);
//    freaqunceCustomPlot->setMinimumSize(300, 300);
//    freaqunceCustomPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom |
//                                         QCP::iSelectAxes | QCP::iSelectLegend
//                                         | QCP::iSelectPlottables);
//    QCPGraph* realTimeGraph =
//        freaqunceCustomPlot->addGraph(wideAxisRect->axis(QCPAxis::atBottom),
//                                      wideAxisRect->axis(QCPAxis::atLeft));
//    QCPGraph* finalGraph =
//        freaqunceCustomPlot->addGraph(subRectLeft->axis(QCPAxis::atBottom),
//                                      subRectLeft->axis(QCPAxis::atLeft));
//    QCPGraph* realTimeDomainGraph = freaqunceCustomPlot->addGraph(
//        timeDomain->axis(QCPAxis::atBottom),
//        timeDomain->axis(QCPAxis::atLeft));
//    layout->addWidget(freaqunceCustomPlot);

//    m_canvas = new RenderArea(this);
//    m_canvas->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
//    layout->addWidget(m_canvas);

//    m_deviceBox = new QComboBox(this);
//    const QAudioDeviceInfo& defaultDeviceInfo =
//        QAudioDeviceInfo::defaultInputDevice();
//    m_deviceBox->addItem(defaultDeviceInfo.deviceName(),
//                         QVariant::fromValue(defaultDeviceInfo));
//    for (auto& deviceInfo :
//         QAudioDeviceInfo::availableDevices(QAudio::AudioInput))
//    {
//        if (deviceInfo != defaultDeviceInfo)
//            m_deviceBox->addItem(deviceInfo.deviceName(),
//                                 QVariant::fromValue(deviceInfo));
//    }

//    connect(m_deviceBox,
//            QOverload<int>::of(&QComboBox::activated),
//            this,
//            &NoiseMeter::deviceChanged);
//    layout->addWidget(m_deviceBox);

//    m_volumeSlider = new QSlider(Qt::Horizontal, this);
//    m_volumeSlider->setRange(0, 100);
//    m_volumeSlider->setValue(100);
//    connect(m_volumeSlider,
//            &QSlider::valueChanged,
//            this,
//            &NoiseMeter::sliderChanged);
//    layout->addWidget(m_volumeSlider);

//    m_modeButton = new QPushButton(this);
//    connect(m_modeButton, &QPushButton::clicked, this,
//    &NoiseMeter::toggleMode); layout->addWidget(m_modeButton);

//    m_suspendResumeButton = new QPushButton(this);
//    connect(m_suspendResumeButton,
//            &QPushButton::clicked,
//            this,
//            &NoiseMeter::toggleSuspend);
//    layout->addWidget(m_suspendResumeButton);

//    window->setLayout(layout);

//    setCentralWidget(window);
//    window->show();
//}

// void NoiseMeter::initializeAudio(const QAudioDeviceInfo& deviceInfo)
//{
//    QAudioFormat format;
//    format.setSampleRate(48000);
//    format.setChannelCount(1);
//    format.setSampleSize(32);
//    format.setSampleType(QAudioFormat::Float);
//    //    format.setByteOrder(QAudioFormat::LittleEndian);
//    format.setCodec("audio/pcm");

//    if (!deviceInfo.isFormatSupported(format))
//    {
//        qWarning() << "Default format not supported - trying to use nearest";
//        format = deviceInfo.nearestFormat(format);
//    }

//    m_audioInfo.reset(new AudioInfo(format));
//    connect(m_audioInfo.data(), &AudioInfo::update, [this]() {
//        m_canvas->setLevel(m_audioInfo->level());
//    });

//    m_audioInput.reset(new QAudioInput(deviceInfo, format));
//    qreal initialVolume = QAudio::convertVolume(m_audioInput->volume(),
//                                                QAudio::LinearVolumeScale,
//                                                QAudio::LogarithmicVolumeScale);
//    m_volumeSlider->setValue(qRound(initialVolume * 100));
//    //    QTimer::singleShot(0, this, &NoiseMeter::toggleMode);
//    //    QTimer::singleShot(0, m_audioInfo.get(), &AudioInfo::start);
//    //    QTimer::singleShot(2000, m_audioInfo.get(), &AudioInfo::stop);
//    //    QTimer::singleShot(10000, this,
//    //    &NoiseMeter::doStaticalDataProcessing);
//    QObject::connect(m_audioInfo.get(),
//                     &AudioInfo::bufferSampled,
//                     this,
//                     &NoiseMeter::doStaticalBufferDataProcessing);
//    QObject::connect(m_audioInfo.get(),
//                     &AudioInfo::audioSampled,
//                     this,
//                     &NoiseMeter::doStaticalAudioDataProcessing);
//    m_audioInfo->start();
//    toggleMode();
//    //    QTimer::singleShot(0, this, [&]() {
//    //        connect(m_audioInfo.get(),
//    //                &QIODevice::aboutToClose,
//    //                this,
//    //                &NoiseMeter::doStaticalDataProcessing);
//    //});
//}

// void NoiseMeter::toggleMode()
//{
//    m_audioInput->stop();
//    toggleSuspend();

//    // Change bewteen pull and push modes
//    if (m_pullMode)
//    {
//        m_modeButton->setText(tr("Enable push mode"));
//        m_audioInput->start(m_audioInfo.data());
//    }
//    else
//    {
//        m_modeButton->setText(tr("Enable pull mode"));
//        auto io = m_audioInput->start();
//        connect(io, &QIODevice::readyRead, [&, io]() {
//            qint64    len        = m_audioInput->bytesReady();
//            const int BufferSize = 2048;
//            if (len > BufferSize)
//                len = BufferSize;

//            QByteArray buffer(len, 0);
//            qint64     l = io->read(buffer.data(), len);
//            if (l > 0)
//                m_audioInfo->write(buffer.constData(), l);
//        });
//    }

//    m_pullMode = !m_pullMode;
//}

// void NoiseMeter::toggleSuspend()
//{
//    // toggle suspend/resume
//    if (m_audioInput->state() == QAudio::SuspendedState ||
//        m_audioInput->state() == QAudio::StoppedState)
//    {
//        m_audioInput->resume();
//        m_suspendResumeButton->setText(tr("Suspend recording"));
//    }
//    else if (m_audioInput->state() == QAudio::ActiveState)
//    {
//        m_audioInput->suspend();
//        m_suspendResumeButton->setText(tr("Resume recording"));
//    }
//    else if (m_audioInput->state() == QAudio::IdleState)
//    {
//        // no-op
//    }
//}

// void NoiseMeter::deviceChanged(int index)
//{
//    m_audioInfo->stop();
//    m_audioInput->stop();
//    m_audioInput->disconnect(this);

//    initializeAudio(m_deviceBox->itemData(index).value<QAudioDeviceInfo>());
//}

// void NoiseMeter::sliderChanged(int value)
//{
//    qreal linearVolume = QAudio::convertVolume(value / qreal(100),
//                                               QAudio::LogarithmicVolumeScale,
//                                               QAudio::LinearVolumeScale);

//    m_audioInput->setVolume(linearVolume);
//}

// void NoiseMeter::doStaticalBufferDataProcessing()
//{

// std::vector<float> signalInFreaqunceRange;
// signalInFreaqunceRange.resize(SIZE);
// std::vector<float>    sampledBuffer = m_audioInfo->getSampledBufferF();
// QVector<QCPGraphData> timeData(SIZE / 2.0);
// const int             size = freaqunceCustomPlot->graph(2)->data()->size();
// for (size_t i = 0; i < SIZE / 2.0; i++)
//{
//    if (i < 0)
//    {
//        //            freaqunceData[i].key   = xAxisValues[i];
//        //            freaqunceData[i].value = 0.0;
//    }
//    else
//    {
//        timeData[i].key   = size + i;
//        timeData[i].value = sampledBuffer[i];
//    }
//}
//    freaqunceCustomPlot->graph(2)->data()->add(timeData);
//    freaqunceCustomPlot->graph(2)->setLineStyle(QCPGraph::lsImpulse);
//    freaqunceCustomPlot->graph(2)->setPen(QPen(QColor("#FFA100"), 1.5));
//    freaqunceCustomPlot->graph(2)->rescaleAxes();
//    freaqunceCustomPlot->replot();

//    for (auto& value : sampledBuffer)
//    {
//        signalOfPartOfSecond.push_back(value);
//    }
//    if (signalOfPartOfSecond.size() == 2048)
//    {
//        // Заменить часть сигнал, умножить на окно произвести преобразование
//        // фурье и отрисовать
//        signalOfLastSecond.erase(signalOfLastSecond.begin(),
//                                 signalOfLastSecond.begin() + 2048);
//        for (auto& value : signalOfPartOfSecond)
//        {
//            signalOfLastSecond.push_back(value);
//        }
//        signalOfPartOfSecond.clear();
//        std::vector<float> signal = signalOfLastSecond;
//        for (int i = 0; i < signal.size(); i++)
//        {
//            signal[i] *= BHWindow[i];
//        }
//        signal.resize(65536);

//        std::cout << "start fourier transfrom" << std::endl << std::flush;
//        QFourierTransformer transformer;
//        // Setting a fixed size for the transformation
//        if (transformer.setSize(65536) == QFourierTransformer::VariableSize)
//        {
//            std::cout
//                << "This size is not a default fixed size of QRealFourier. "
//                   "Using a variable size instead."
//                << std::endl
//                << std::flush;
//        }
//        else if (transformer.setSize(65536) ==
//        QFourierTransformer::InvalidSize)
//        {
//            std::cout << "Invalid FFT size." << std::endl << std::flush;
//            return;
//        }
//        std::vector<float> signalOfLastSecondInFreaqunceRange;
//        signalOfLastSecondInFreaqunceRange.resize(65536);
//        transformer.forwardTransform(
//            const_cast<float*>(signal.data()),
//            const_cast<float*>(signalOfLastSecondInFreaqunceRange.data()));
//        std::cout << "fourier transforming finished." << std::endl
//                  << std::flush;
//        QComplexVector complexVectorValues =
//            transformer.toComplex(signalOfLastSecondInFreaqunceRange.data());
//        std::cout
//            << "complex transforming finished, start transform data fo
//            drawning"
//            << std::endl
//            << std::flush;

//        QVector<QCPGraphData> freaqunceData(48000 / 2.0);
//        float                 prevValue;
//        float                 currValue;
//        float                 step = 0.37;
//        for (size_t i = 0; i < 48000 / 2.0; i++)
//        {
//            if (i < 0)
//            {
//                //            freaqunceData[i].key   = xAxisValues[i];
//                //            freaqunceData[i].value = 0.0;
//            }
//            else
//            {
//                if (i < 128)
//                {
//                    freaqunceData[i].key = i * 0.37;
//                }
//                else if (i < 192)
//                {
//                    freaqunceData[i].key = 128 * 0.37 + (i - (192 - 65)) *
//                    0.74;
//                }
//                else if (i < 256)
//                {
//                    freaqunceData[i].key =
//                        128 * 0.37 + 64 * 0.74 + (i - (256 - 65)) * 1.48;
//                }
//                else if (i < 320)
//                {
//                    freaqunceData[i].key = 128 * 0.37 + 64 * 0.74 + 64 * 1.48
//                    +
//                                           (i - (320 - 65)) * 2.96;
//                }
//                else if (i < 384)
//                {
//                    freaqunceData[i].key = 128 * 0.37 + 64 * 0.74 + 64 * 1.48
//                    +
//                                           64 * 2.96 + (i - (384 - 65))
//                                           * 5.92;
//                }
//                else if (i < 448)
//                {
//                    freaqunceData[i].key = 128 * 0.37 + 64 * 0.74 + 64 * 1.48
//                    +
//                                           64 * 2.96 + 64 * 5.92 +
//                                           (i - (448 - 65)) * 11.84;
//                }
//                else if (i < 512)
//                {
//                    freaqunceData[i].key = 128 * 0.37 + 64 * 0.74 + 64 * 1.48
//                    +
//                                           64 * 2.96 + 64 * 5.92 + 64 * 11.84
//                                           + (i - (512 - 65)) * 23.68;
//                }
//                else if (i < 576)
//                {
//                    freaqunceData[i].key = 128 * 0.37 + 64 * 0.74 + 64 * 1.48
//                    +
//                                           64 * 2.96 + 64 * 5.92 + 64 * 11.84
//                                           + 64 * 23.68 + (i - (576 - 65))
//                                           * 47.28;
//                }
//                else
//                {
//                    freaqunceData[i].key = 128 * 0.37 + 64 * 0.74 + 64 * 1.48
//                    +
//                                           64 * 2.96 + 64 * 5.92 + 64 * 11.84
//                                           + 64 * 23.68 + 64 * 47.28 + (i -
//                                           (24000 - 192)) * 94.56;
//                }

//                //            std::pow(2, m_format.sampleSize())
//                float real      = complexVectorValues[i].real();
//                float imaginary = complexVectorValues[i].imaginary();
//                float amplitude =
//                    std::sqrt(real * real + imaginary * imaginary);
//                qreal valueIndB =
//                    QAudio::convertVolume(amplitude,
//                                          QAudio::LinearVolumeScale,
//                                          QAudio::DecibelVolumeScale);
//                //            valueIndB += 200.0;
//                freaqunceData[i].value = valueIndB;
//            }
//        }
//        std::cout << "draw data" << std::endl << std::flush;
//        //    freaqunceCustomPlot->graph(0)->data().clear();
//        freaqunceCustomPlot->graph(1)->data()->set(freaqunceData);
//        freaqunceCustomPlot->graph(1)->setLineStyle(QCPGraph::lsImpulse);
//        freaqunceCustomPlot->graph(1)->setPen(QPen(QColor("#FFA100"), 1.5));
//        //        scaleAxisToEachOther();
//        freaqunceCustomPlot->graph(1)->rescaleAxes();
//        freaqunceCustomPlot->replot();
//    }
//    else
//    {
//        // TODO (продолжаем копить данные)
//    }
//}

// void NoiseMeter::doStaticalAudioDataProcessing()
//{
//    std::vector<float> signalInFreaqunceRange;
//    signalInFreaqunceRange.resize(131072);
//    std::vector<float> sampledAudio = m_audioInfo->getSampledAudioF();
//    if (sampledAudio.size() < 131072)
//    {
//        for (int i = sampledAudio.size(); i < 131072; i++)
//        {
//            sampledAudio.push_back(0.0);
//        }
//    }

//    const auto  h                   = buildCoreLPFilt(0.2, 0.02);
//    const auto& resultSampledBuffer = convolve(sampledAudio, h);

//    std::cout << "start fourier transfrom" << std::endl << std::flush;
//    QFourierTransformer transformer;
//    // Setting a fixed size for the transformation
//    if (transformer.setSize(131072) == QFourierTransformer::VariableSize)
//    {
//        std::cout << "This size is not a default fixed size of QRealFourier. "
//                     "Using a variable size instead."
//                  << std::endl
//                  << std::flush;
//    }
//    else if (transformer.setSize(131072) == QFourierTransformer::InvalidSize)
//    {
//        std::cout << "Invalid FFT size." << std::endl << std::flush;
//        return;
//    }
//    transformer.forwardTransform(
//        const_cast<float*>(resultSampledBuffer.data()),
//        const_cast<float*>(signalInFreaqunceRange.data()));
//    QComplexVector complexVectorValues =
//        transformer.toComplex(signalInFreaqunceRange.data());

//    QVector<QCPGraphData> freaqunceData(98304 / 2.0);
//    for (size_t i = 0; i < 98304 / 2.0; i++)
//    {
//        if (i < 0)
//        {
//            //            freaqunceData[i].key   = xAxisValues[i];
//            //            freaqunceData[i].value = 0.0;
//        }
//        else
//        {
//            freaqunceData[i].key = i / 2.0;
//            //            std::pow(2, m_format.sampleSize())
//            float real      = complexVectorValues[i].real();
//            float imaginary = complexVectorValues[i].imaginary();
//            float amplitude = std::sqrt(real * real + imaginary * imaginary);
//            qreal valueIndB = QAudio::convertVolume(amplitude,
//                                                    QAudio::LinearVolumeScale,
//                                                    QAudio::DecibelVolumeScale);
//            //            valueIndB += 200.0;
//            freaqunceData[i].value = valueIndB;
//        }
//    }
//    //    freaqunceCustomPlot->graph(0)->data().clear();
//    freaqunceCustomPlot->graph(0)->data()->set(freaqunceData);
//    freaqunceCustomPlot->graph(0)->setLineStyle(QCPGraph::lsImpulse);
//    freaqunceCustomPlot->graph(0)->setPen(QPen(QColor("#FFA100"), 1.5));
//    freaqunceCustomPlot->graph(0)->rescaleAxes();
//    freaqunceCustomPlot->replot();
//}

// bool NoiseMeter::buildBlackmanHarrisWindow(std::vector<float>& window)
//{
//    const float a0 = 0.35875f;
//    const float a1 = 0.48829f;
//    const float a2 = 0.14128f;
//    const float a3 = 0.01168f;

//    unsigned int idx = 0;
//    size_t       num = window.size();
//    while (idx < num)
//    {
//        window[idx] = a0 - (a1 * cosf((2.0f * M_PI * idx) / (num - 1))) +
//                      (a2 * cosf((4.0f * M_PI * idx) / (num - 1))) -
//                      (a3 * cosf((6.0f * M_PI * idx) / (num - 1)));
//        idx++;
//    }
//    return true;
//}

// std::vector<float> NoiseMeter::buildCoreLPFilt(double fc, double BW)
//{
//    int                m = 4.0 / BW;
//    double             k = 0.49;
//    std::vector<float> h(static_cast<int>(m), 1.0f);
//    std::vector<float> window(m);
//    buildBlackmanHarrisWindow(window);
//    for (int i = 0; i < m; i++)
//    {
//        if (i == m / 2)
//        {
//            h[i] = h[i] * (2 * M_PI * fc * k);
//        }
//        else
//        {
//            h[i] =
//                h[i] * (k * (sin(2 * M_PI * fc * (i - m / 2)) / (i - m / 2)));
//            h[i] = h[i] * window[i];
//        }
//    }
//    return h;
//}

// std::vector<float> NoiseMeter::buildCoreHPFilt(double fc, double BW)
//{
//    std::vector<float> LP_FILT = buildCoreLPFilt(fc, BW);
//    int                i       = 0;
//    for (auto& value : LP_FILT)
//    {
//        if (i % 2 == 0.0)
//        {
//            value = -value;
//        }
//        else
//        {
//        }
//        i++;
//    }
//    LP_FILT[LP_FILT.size() / 2.0 - 1]++;
//    LP_FILT.pop_back();

//    return LP_FILT;
//}

// std::vector<float> NoiseMeter::convolve(const std::vector<float>& u,
//                                        const std::vector<float>& v)
//{
//    int                m = u.size();
//    int                n = v.size();
//    std::vector<float> w(m + n - 1);
//    for (int k = 0; k < w.size(); k++)
//    {
//        w[k] = 0;

//        for (int i = std::max(0, k - n + 1); i < std::min(k + 1, m); i++)
//            w[k] += u[i] * v[k - i];
//    }
//    for (int i = 0; i < n - 1; i++)
//    {
//        w.pop_back();
//    }

//    return w;

//    //    auto   signalCopy      = signal;
//    //    size_t impResponseSize = impulseResponse.size();
//    //    for (int i = 0; i < impResponseSize; i++)
//    //    {
//    //        signalCopy.push_back(0);
//    //    }
//    //    size_t             signalSize = signal.size();
//    //    std::vector<float> result;
//    //    for (int i = 0; i < signalSize; i++)
//    //    {
//    //        std::vector<float> tmpVector;
//    //        for (int j = i; j < impResponseSize; j++)
//    //        {
//    //            tmpVector.push_back(signal[j] * impulseResponse[j - i]);
//    //        }
//    //        float resultValue = 0.0;
//    //        for (int j = 0; j < tmpVector.size(); j++)
//    //        {
//    //            resultValue += tmpVector[i];
//    //        }
//    //        result.push_back(resultValue);
//    //    }
//    //    //    for (int i = 0; i < impResponseSize; i++)
//    //    //    {
//    //    //        result.pop_back();
//    //    //    }
//    //    return result;
//}

// void NoiseMeter::scaleAxisToEachOther()
//{
//    // для приведения 1:1 x и y
//    //  qDebug() << "before" << xAxis->range().size() << yAxis->range().size()
//    //           << (xAxis->range().size() > yAxis->range().size());

//    if (freaqunceCustomPlot->xAxis->range().size() >
//        freaqunceCustomPlot->yAxis->range().size())
//    {
//        //    xAxis->setScaleRatio(yAxis);
//        //    if (xAxis->axisRect()->width() > yAxis->axisRect()->height()) {
//        //    } else {
//        if (freaqunceCustomPlot->yAxis->range().size() >
//            freaqunceCustomPlot->xAxis->range().size() / 2.0)
//        {
//            freaqunceCustomPlot->xAxis->setScaleRatio(
//                freaqunceCustomPlot->yAxis, 1.0);
//        }
//        else
//        {
//            freaqunceCustomPlot->yAxis->setScaleRatio(
//                freaqunceCustomPlot->xAxis, 1.0);
//        }
//        //    }

//        //    yAxis->rescale();
//    }
//    else
//    {
//        //    if (xAxis->axisRect()->width() > yAxis->axisRect()->height()) {
//        freaqunceCustomPlot->xAxis->setScaleRatio(freaqunceCustomPlot->yAxis,
//                                                  1.0);
//        //    } else {
//        //      yAxis->setScaleRatio(xAxis, 1.0);
//        //    }
//        //    xAxis->rescale();
//    }
//    //  qDebug() << "after" << xAxis->range().size() << yAxis->range().size()
//    //           << (xAxis->range().size() > yAxis->range().size());
//}
