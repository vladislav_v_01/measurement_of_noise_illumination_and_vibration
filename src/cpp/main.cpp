#include <QApplication>
#include <QAudioInput>
#include <QFontDatabase>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

#include "luxmeterworker.h"
#include "qcustomplotqml.h"

#include <limits>

#include "NoiseMeter.hpp"
//#include "mainwindow.h"

#include "globalworker.h"
#include "singleton.h"

int main(int argc, char* argv[])
{
    QApplication::setApplicationName("Sound level meter");
    QApplication::setOrganizationName("BSUIR");

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    //        qmlRegisterSingletonType("LuxMeterLib", 1, 0, "LuxMeter");
    qmlRegisterType<CustomPlotItem>("CustomPlot", 1, 0, "CustomPlotItem");

    QQuickStyle::setStyle("Material");

    QFontDatabase fontDatabase;
    if (fontDatabase.addApplicationFont(
            ":/fonts/Font Awesome 5 Free-Solid-900.otf") == -1)
        qWarning() << "Failed to load Font Awesome 5 Free-Solid-900.otf";

    engine.rootContext()->setContextProperty(
        QStringLiteral("luxMeterWorker"),
        SINGLETON(GlobalWorker)->getLuxMeterWorker().get());
    engine.rootContext()->setContextProperty(
        QStringLiteral("noiseMeterWorker"),
        SINGLETON(GlobalWorker)->getNoiseMeterWorker().get());

    engine.load(QUrl("qrc:/qml/MainWindow.qml"));

    //    SINGLETON(GlobalWorker)->get_luxMeter()->set_customPlot();

    return app.exec();
}
