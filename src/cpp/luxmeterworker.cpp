#include "luxmeterworker.h"

#include <QtGlobal>

#include "qcustomplot.h"

#include <iostream>

LuxMeterWorker::LuxMeterWorker(QObject* p_parent)
    : QObject(p_parent)
    , m_luxMeter(new LuxMeter(this))
    , m_customPlot(nullptr)
    , m_currValueLabel(nullptr)
    , m_currUnitLabel(nullptr)
    , m_minValueLabel(nullptr)
    , m_avgValueLabel(nullptr)
    , m_maxValueLabel(nullptr)
    , m_summOfMeasurings(0)
    , m_countOfMeasurings(0)
    , m_maxValueForRF(-1.0)
    , m_minValueForRF(99999.0)
    , m_rippleFactorTimer(new QTimer)
{
    m_rippleFactorTimer->setInterval(5000);
    QObject::connect(m_rippleFactorTimer,
                     &QTimer::timeout,
                     this,
                     &LuxMeterWorker::onRFTimerTimeout);
    m_rippleFactorTimer->start();
}

LuxMeterWorker::~LuxMeterWorker() {}

void LuxMeterWorker::onStopStartButtonClicked()
{
    const auto& currText = this->m_startStopButton->property("text");
    const auto& newText  = currText == "\uF04C" ? "\uF04B" : "\uF04C";
    this->m_startStopButton->setProperty("text", newText);
    newText == QString("\uF04C") ? this->m_luxMeter->startListening()
                                 : this->m_luxMeter->stopListening();
}

void LuxMeterWorker::onResetButtonClicked()
{
    this->m_currValueLabel->setProperty(
        "text", QString(this->m_countOFSymbolsOnValueLabels, '0'));
    this->m_minValueLabel->setProperty(
        "text", QString(this->m_countOFSymbolsOnValueLabels, '0'));
    this->m_avgValueLabel->setProperty(
        "text", QString(this->m_countOFSymbolsOnValueLabels, '0'));
    this->m_countOfMeasurings = 0;
    this->m_summOfMeasurings  = 0;
    this->m_maxValueLabel->setProperty(
        "text", QString(this->m_countOFSymbolsOnValueLabels, '0'));
    this->get_customPlot()->customPlot()->graph(0)->data()->clear();
    this->get_customPlot()->customPlot()->xAxis->setRange(
        0, 60, Qt::AlignRight);
    this->get_customPlot()->update();
}

void LuxMeterWorker::onIncreaseButtonClicked()
{
    double newUpperRange = 0.0;
    if (m_currYAxisRangeUpper == 100)
    {
        newUpperRange = 250;
    }
    else if (m_currYAxisRangeUpper == 250)
    {
        newUpperRange = 500;
        ;
    }
    else if (m_currYAxisRangeUpper == 500)
    {
        newUpperRange = 1000;
    }
    else if (m_currYAxisRangeUpper == 1000)
    {
        newUpperRange = 2000;
    }
    else if (m_currYAxisRangeUpper == 2000)
    {
        newUpperRange = 5000;
    }
    else if (m_currYAxisRangeUpper == 5000)
    {
        newUpperRange = 10000;
    }
    else if (m_currYAxisRangeUpper == 10000)
    {
        newUpperRange = 20000;
    }
    else if (m_currYAxisRangeUpper == 20000)
    {
        newUpperRange = 35000;
    }
    else if (m_currYAxisRangeUpper == 35000)
    {
        newUpperRange = 35000;
    }
    else
    {
        newUpperRange = 250;
    }
    m_currYAxisRangeUpper = newUpperRange;
    this->get_customPlot()->customPlot()->yAxis->setRangeUpper(newUpperRange);
    this->get_customPlot()->update();
}

void LuxMeterWorker::onReduceButtonClicked()
{
    double newUpperRange = 0.0;
    if (m_currYAxisRangeUpper == 100)
    {
        newUpperRange = 100;
    }
    else if (m_currYAxisRangeUpper == 250)
    {
        newUpperRange = 100;
        ;
    }
    else if (m_currYAxisRangeUpper == 500)
    {
        newUpperRange = 250;
    }
    else if (m_currYAxisRangeUpper == 1000)
    {
        newUpperRange = 500;
    }
    else if (m_currYAxisRangeUpper == 2000)
    {
        newUpperRange = 1000;
    }
    else if (m_currYAxisRangeUpper == 5000)
    {
        newUpperRange = 2000;
    }
    else if (m_currYAxisRangeUpper == 10000)
    {
        newUpperRange = 5000;
    }
    else if (m_currYAxisRangeUpper == 20000)
    {
        newUpperRange = 10000;
    }
    else if (m_currYAxisRangeUpper == 35000)
    {
        newUpperRange = 20000;
    }
    else
    {
        newUpperRange = 250;
    }
    m_currYAxisRangeUpper = newUpperRange;
    this->get_customPlot()->customPlot()->yAxis->setRangeUpper(newUpperRange);
    this->get_customPlot()->update();
}

void LuxMeterWorker::onUnitButtonClicked()
{
    const auto& newUnitLabelText =
        this->m_currUnitLabel->property("text") == "Lx" ? "Fc" : "Lx";
    const auto& newChangeUnitButtonText =
        this->m_changeUnitsButton->property("text") == "Lx" ? "Fc" : "Lx";
    //    isLuxUnits = (this->m_currUnitLabel->property("text") == "Fc") ? true
    //    : false;
    this->m_currUnitLabel->setProperty("text", newUnitLabelText);
    this->m_changeUnitsButton->setProperty("text", newChangeUnitButtonText);
}

void LuxMeterWorker::onItemCompleted(QQuickItem* newItem,
                                     const char* newParameter)
{
    Q_ASSERT(newItem != nullptr);
    const auto& itemName = newItem->objectName();
    if (itemName == "minValueLabel")
    {
        this->m_minValueLabel = newItem;
    }
    else if (itemName == "avgValueLabel")
    {
        this->m_avgValueLabel = newItem;
    }
    else if (itemName == "maxValueLabel")
    {
        this->m_maxValueLabel = newItem;
    }
    else if (itemName == "rippleFactorValueLabel")
    {
        this->m_rippleFactorLabel = newItem;
    }
    else if (itemName == "currValueLabel")
    {
        this->m_currValueLabel = newItem;
        QObject::connect(this->m_luxMeter,
                         &LuxMeter::newValue,
                         this,
                         &LuxMeterWorker::onNewValue);
        this->m_luxMeter->startListening();
    }
    else if (itemName == "luxmeterPlot")
    {
        this->set_customPlot(newItem->findChild<CustomPlotItem*>());
    }
    else if (itemName == "startStopButton")
    {
        this->m_startStopButton =
            newItem->findChild<QObject*>("flickableButtonText");
    } /*else if(itemName == "currValueUnit") {
        this->m_currUnitLabel = newItem;
    } else if(itemName == "changeUnitsButton") {
        this->m_changeUnitsButton =
        id: contentText;newItem->findChild<QObject
    *>("flickableButtonText");
    }*/
}

void LuxMeterWorker::onItemDestroyed(QQuickItem* newItem)
{
    Q_ASSERT(newItem != nullptr);
    const auto& itemName = newItem->objectName();
    if (itemName == "minValueLabel")
    {
        this->m_minValueLabel = nullptr;
    }
    else if (itemName == "avgValueLabel")
    {
        this->m_avgValueLabel     = nullptr;
        this->m_summOfMeasurings  = 0.0;
        this->m_countOfMeasurings = 0;
    }
    else if (itemName == "maxValueLabel")
    {
        this->m_maxValueLabel = nullptr;
    }
    else if (itemName == "currValueLabel")
    {
        this->m_currValueLabel = nullptr;
        QObject::disconnect(this->m_luxMeter,
                            &LuxMeter::newValue,
                            this,
                            &LuxMeterWorker::onNewValue);
        this->m_luxMeter->startListening();
    }
    else if (itemName == "luxmeterPlot")
    {
        this->get_customPlot()->customPlot()->graph(0)->data()->clear();
        this->get_customPlot() = nullptr;
    } /*else if(itemName =="currValueUnit") {
        this->m_currUnitLabel = nullptr;
    } else if(itemName == "changeUnitsButton") {
        this->m_changeUnitsButton = nullptr;
    }*/
}

void LuxMeterWorker::onNewValue(qreal newLuxValue)
{
    this->m_summOfMeasurings += newLuxValue;
    this->m_countOfMeasurings++;
    QString newValueText = QString::number(newLuxValue, 'f', 0);
    int     diff         = m_countOFSymbolsOnValueLabels - newValueText.count();

    m_maxValueForRF =
        m_maxValueForRF > newLuxValue ? m_maxValueForRF : newLuxValue;
    //    qDebug() << m_maxValueForRF << Qt::endl;
    m_minValueForRF =
        m_minValueForRF < newLuxValue ? m_minValueForRF : newLuxValue;
    //    qDebug() << m_minValueForRF << Qt::endl;
    m_summValuesForRF += newLuxValue;
    //    qDebug() << m_summValuesForRF << Qt::endl;

    const auto newRF = (m_maxValueForRF - m_minValueForRF) /
                       (2.0 * m_summValuesForRF / (++m_countOfMeasuringsRF)) *
                       100;
    //    qDebug() << newRF << Qt::endl;

    if (this->m_currValueLabel != nullptr)
    {
        this->m_currValueLabel->setProperty("text",
                                            QString(diff, '0') + newValueText);
    }
    if (this->m_rippleFactorLabel != nullptr)
    {
        this->m_rippleFactorLabel->setProperty("text",
                                               QString::number((int)newRF));
    }
    if (this->m_avgValueLabel != nullptr)
    {
        auto    avgValue = this->m_summOfMeasurings / this->m_countOfMeasurings;
        QString avgValueText = QString::number(avgValue, 'f', 0);
        int     diffForAvgValue =
            m_countOFSymbolsOnValueLabels - avgValueText.count();
        this->m_avgValueLabel->setProperty(
            "text", QString(diffForAvgValue, '0') + avgValueText);
    }
    if (this->m_minValueLabel != nullptr)
    {
        QString currMinValue =
            this->m_minValueLabel->property("text").toString();
        QString newValue = QString(diff, '0') + newValueText;
        this->m_minValueLabel->setProperty(
            "text",
            qFuzzyIsNull(currMinValue.toDouble())
                ? newValue
                : (currMinValue.toDouble() < newValue.toDouble() ? currMinValue
                                                                 : newValue));
    }
    if (this->m_maxValueLabel != nullptr)
    {
        QString currMaxValue =
            this->m_maxValueLabel->property("text").toString();
        QString newValue = QString(diff, '0') + newValueText;
        this->m_maxValueLabel->setProperty(
            "text",
            currMaxValue.toDouble() > newValue.toDouble() ? currMaxValue
                                                          : newValue);
    }
    if (this->get_customPlot() != nullptr)
    {
        double key =
            this->get_customPlot()->customPlot()->graph(0)->data()->isEmpty()
                ? 0.0
                : (this->get_customPlot()
                       ->customPlot()
                       ->graph(0)
                       ->data()
                       ->end() -
                   1)
                          ->key +
                      0.5;
        this->get_customPlot()->customPlot()->graph(0)->addData(key,
                                                                newLuxValue);
        this->get_customPlot()->customPlot()->xAxis->rescale();
        this->get_customPlot()->customPlot()->graph(0)->rescaleValueAxis(false,
                                                                         true);
        this->get_customPlot()->customPlot()->xAxis->setRange(
            this->get_customPlot()->customPlot()->xAxis->range().upper,
            60,
            Qt::AlignRight);
        this->get_customPlot()->customPlot()->yAxis->setRange(
            0, m_currYAxisRangeUpper);
        this->get_customPlot()->customPlot()->replot();
        this->get_customPlot()->update();
    }
}

void LuxMeterWorker::onNewRFMeasuringTime(QString newMeasuringTime)
{
    if (newMeasuringTime == "5 s")
    {
        m_rippleFactorTimer->setInterval(5000);
    }
    else if (newMeasuringTime == "10 s")
    {
        m_rippleFactorTimer->setInterval(10000);
    }
    else if (newMeasuringTime == "20 s")
    {
        m_rippleFactorTimer->setInterval(20000);
    }
    else if (newMeasuringTime == "30 s")
    {
        m_rippleFactorTimer->setInterval(30000);
    }
    else if (newMeasuringTime == "40 s")
    {
        m_rippleFactorTimer->setInterval(40000);
    }
    else if (newMeasuringTime == "50 s")
    {
        m_rippleFactorTimer->setInterval(50000);
    }
    else if (newMeasuringTime == "60 s")
    {
        m_rippleFactorTimer->setInterval(60000);
    }
}

void LuxMeterWorker::onRFTimerTimeout()
{
    m_maxValueForRF       = -1.0;
    m_minValueForRF       = 99999.0;
    m_summValuesForRF     = -1.0;
    m_countOfMeasuringsRF = 0;
}
