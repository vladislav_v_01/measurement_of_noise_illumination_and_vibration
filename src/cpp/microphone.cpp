#include "microphone.h"

#include <iostream>

#include "appsettings.h"
#include "singleton.h"

#include <QtConcurrent/QtConcurrent>
#include <QtEndian>

#include "libs/QRealFourier/headers/qfouriertransformer.h"

Microphone::Microphone(QObject* parent)
    : QIODevice(parent)
{
    initializeAudio(QAudioDeviceInfo::defaultInputDevice());
    valuesForLastSecond.resize(SINGLETON(AppSettings)->get_sampleRate());
}

Microphone::~Microphone() {}

void Microphone::initializeAudio(const QAudioDeviceInfo& deviceInfo)
{
    const auto& codecsList        = deviceInfo.supportedCodecs();
    const auto& sampleRatesList   = deviceInfo.supportedSampleRates();
    const auto& channelCountsList = deviceInfo.supportedChannelCounts();
    const auto& sampleSizesList   = deviceInfo.supportedSampleSizes();
    const auto& sampleTypesList   = deviceInfo.supportedSampleTypes();
    const auto& byteOrdersList    = deviceInfo.supportedByteOrders();

    m_format.setSampleRate(SINGLETON(AppSettings)->get_sampleRate());
    m_format.setChannelCount(SINGLETON(AppSettings)->get_channelCount());
    m_format.setSampleSize(SINGLETON(AppSettings)->get_sampleSize());
    m_format.setSampleType(SINGLETON(AppSettings)->get_sampleType());
    //    m_format.setByteOrder(QAudioFormat::LittleEndian);
    m_format.setCodec("audio/pcm");

    if (!deviceInfo.isFormatSupported(m_format))
    {
        std::cerr << Q_FUNC_INFO << __LINE__
                  << "Default format not supported - trying to use nearest"
                  << std::endl;
        m_format = deviceInfo.nearestFormat(m_format);
    }

    initAudioInfo();
    BlackmanHarrisWidnow.resize(SINGLETON(AppSettings)->get_sampleRate());
    buildBlackmanHarrisWindow(BlackmanHarrisWidnow);
    //    m_audioInfo = std::make_unique<AudioInfo>(format);
    //    connect(m_audioInfo.get(),
    //            &AudioInfo::bufferSampled,
    //            this,
    //            &Microphone::bufferTransfer);
    this->start();
    m_audioInput = std::make_unique<QAudioInput>(deviceInfo, m_format);
    toggleMode();
}

void Microphone::start()
{
    open(QIODevice::WriteOnly);
}

void Microphone::stop()
{
    close();
}

void Microphone::toggleMode()
{
    m_audioInput->stop();
    toggleSuspend();

    // Change bewteen pull and push modes
    if (m_pullMode)
    {
        //        m_modeButton->setText(tr("Enable push mode"));
        m_audioInput->start(this);
    }
    else
    {
        //        m_modeButton->setText(tr("Enable pull mode"));
        auto io = m_audioInput->start();
        connect(io, &QIODevice::readyRead, [&, io]() {
            qint64    len        = m_audioInput->bytesReady();
            const int BufferSize = 2048;
            if (len > BufferSize)
                len = BufferSize;

            QByteArray buffer(len, 0);
            qint64     l = io->read(buffer.data(), len);
            if (l > 0)
                this->write(buffer.constData(), l);
        });
    }

    m_pullMode = !m_pullMode;
}

void Microphone::toggleSuspend()
{
    // toggle suspend/resume
    if (m_audioInput->state() == QAudio::SuspendedState ||
        m_audioInput->state() == QAudio::StoppedState)
    {
        m_audioInput->resume();
        //        m_suspendResumeButton->setText(tr("Suspend recording"));
    }
    else if (m_audioInput->state() == QAudio::ActiveState)
    {
        m_audioInput->suspend();
        //        m_suspendResumeButton->setText(tr("Resume recording"));
    }
    else if (m_audioInput->state() == QAudio::IdleState)
    {
        // no-op
    }
}

qint64 Microphone::readData(char* data, qint64 maxlen)
{
    Q_UNUSED(data)
    Q_UNUSED(maxlen)

    return 0;
}

qint64 Microphone::writeData(const char* data, qint64 len)
{
    if (m_maxAmplitude)
    {
        Q_ASSERT(m_format.sampleSize() % 8 == 0);
        const int channelBytes = m_format.sampleSize() / 8;
        const int sampleBytes  = m_format.channelCount() * channelBytes;
        Q_ASSERT(len % sampleBytes == 0);
        const int numSamples = len / sampleBytes;

        quint32              maxValue = 0;
        const unsigned char* ptr = reinterpret_cast<const unsigned char*>(data);

        std::vector<float> sampledBuffer;
        for (int i = 0; i < numSamples; ++i)
        {
            for (int j = 0; j < m_format.channelCount(); ++j)
            {
                quint32 value   = 0;
                float   value_f = 0.0;

                if (m_format.sampleSize() == 8 &&
                    m_format.sampleType() == QAudioFormat::UnSignedInt)
                {
                    value = *reinterpret_cast<const quint8*>(ptr);
                }
                else if (m_format.sampleSize() == 8 &&
                         m_format.sampleType() == QAudioFormat::SignedInt)
                {
                    value = qAbs(*reinterpret_cast<const qint8*>(ptr));
                }
                else if (m_format.sampleSize() == 16 &&
                         m_format.sampleType() == QAudioFormat::UnSignedInt)
                {
                    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
                        value = qFromLittleEndian<quint16>(ptr);
                    else
                        value = qFromBigEndian<quint16>(ptr);
                }
                else if (m_format.sampleSize() == 16 &&
                         m_format.sampleType() == QAudioFormat::SignedInt)
                {
                    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
                        value = qAbs(qFromLittleEndian<qint16>(ptr));
                    else
                        value = qAbs(qFromBigEndian<qint16>(ptr));
                }
                else if (m_format.sampleSize() == 32 &&
                         m_format.sampleType() == QAudioFormat::UnSignedInt)
                {
                    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
                        value = qFromLittleEndian<quint32>(ptr);
                    else
                        value = qFromBigEndian<quint32>(ptr);
                }
                else if (m_format.sampleSize() == 32 &&
                         m_format.sampleType() == QAudioFormat::SignedInt)
                {
                    if (m_format.byteOrder() == QAudioFormat::LittleEndian)
                        value = qAbs(qFromLittleEndian<qint32>(ptr));
                    else
                        value = qAbs(qFromBigEndian<qint32>(ptr));
                }
                else if (m_format.sampleSize() == 32 &&
                         m_format.sampleType() == QAudioFormat::Float)
                {
                    value   = qAbs(*reinterpret_cast<const float*>(ptr) *
                                 0x7fffffff); // assumes 0-1.0
                    value_f = (*reinterpret_cast<const float*>(ptr));
                }

                //                qreal valueIndB =
                //                    QAudio::convertVolume(value /*/
                //                    m_maxAmplitude*/,
                //                                          QAudio::LogarithmicVolumeScale,
                //                                          QAudio::DecibelVolumeScale);

                valuesForTwoSeconds.push_back(value_f);

                sampledBuffer.push_back(value_f);

                maxValue = qMax(value, maxValue);
                ptr += channelBytes;
            }
        }

#ifdef NEED_DEBUG_INFO
        auto maxAudioLevel =
            std::max_element(m_sampledAudio.begin(), m_sampledAudio.end());

        m_maxAudioLevel = *maxAudioLevel;
#endif
        maxValue = qMin(maxValue, m_maxAmplitude);
        QtConcurrent::run(
            this, &Microphone::doStaticalDataProcessing, sampledBuffer);
        if (valuesForTwoSeconds.size() == 131072)
        {
            QtConcurrent::run(
                this,
                &Microphone::doStaticalDataProcessingForTwoSeconds,
                valuesForTwoSeconds);
            valuesForTwoSeconds.clear();
        }
        //        emit bufferSampled(sampledBuffer);
    }
    return len;
}

std::mutex g_mutex;

void Microphone::doStaticalDataProcessing(std::vector<float> sampledBuffer)
{
    const size_t          sampledBufferSize = sampledBuffer.size();
    QVector<QCPGraphData> dataInTimeDomain;
    dataInTimeDomain.resize(sampledBufferSize);
    {
        std::lock_guard<std::mutex> lock(g_mutex);
        for (size_t i = 0; i < sampledBufferSize; i++)
        {
            dataInTimeDomain[i].key   = i;
            dataInTimeDomain[i].value = sampledBuffer[i];
            partOfValuesForLastSecond.push_back(sampledBuffer[i]);
        }

        emit drawTimeDomain(dataInTimeDomain);
        dataInTimeDomain.clear();

        // 4096 -- количество отчётов, через которое будет выполнено новое
        // преобразование
        const size_t bufferSize =
            static_cast<size_t>(SINGLETON(AppSettings)->get_bufferSize());
        if (partOfValuesForLastSecond.size() == bufferSize)
        {
            // Заменить часть сигнала, умножить на окно произвести
            // преобразование фурье и вызвать сигнал на отрисовку
            valuesForLastSecond.erase(valuesForLastSecond.begin(),
                                      valuesForLastSecond.begin() + bufferSize);
            for (auto& value : partOfValuesForLastSecond)
            {
                valuesForLastSecond.push_back(value);
            }
            partOfValuesForLastSecond.clear();
            std::vector<float> values = valuesForLastSecond;
            for (int i = 0; i < values.size(); i++)
            {
                values[i] *= BlackmanHarrisWidnow[i];
            }
            // 65536 -- ближайшее значение большее 48000, которое является
            // степенью двойки
            values.resize(65536);

            //            std::cout << "start fourier transfrom" << std::endl <<
            //            std::flush;
            QFourierTransformer transformer;
            // Setting a fixed size for the transformation
            if (transformer.setSize(65536) == QFourierTransformer::VariableSize)
            {
                //                std::cout
                //                    << "This size is not a default fixed size
                //                    of QRealFourier. "
                //                       "Using a variable size instead."
                //                    << std::endl
                //                    << std::flush;
            }
            else if (transformer.setSize(65536) ==
                     QFourierTransformer::InvalidSize)
            {
                std::cout << "Invalid FFT size." << std::endl << std::flush;
                return;
            }
            std::vector<float> signalOfLastSecondInFreaqunceRange;
            signalOfLastSecondInFreaqunceRange.resize(65536);
            transformer.forwardTransform(
                const_cast<float*>(values.data()),
                const_cast<float*>(signalOfLastSecondInFreaqunceRange.data()));
            //            std::cout << "fourier transforming finished." <<
            //            std::endl
            //                      << std::flush;
            QComplexVector complexVectorValues = transformer.toComplex(
                signalOfLastSecondInFreaqunceRange.data());
            //            std::cout
            //                << "complex transforming finished, start transform
            //                data fo"
            //                   "drawning "
            //                << std::endl
            //                << std::flush;

            QVector<QCPGraphData> frequencyData(65536 / 2.0);
            float                 step = 24000.0 / 32768.0;
            for (size_t i = 0; i < 65536 / 2.0; i++)
            {
                frequencyData[i].key = i * step;
                float real           = complexVectorValues[i].real();
                float imaginary      = complexVectorValues[i].imaginary();
                float amplitude =
                    std::sqrt(real * real + imaginary * imaginary);
                qreal valueIndB =
                    QAudio::convertVolume(amplitude,
                                          QAudio::LinearVolumeScale,
                                          QAudio::DecibelVolumeScale);
                frequencyData[i].value = valueIndB;
            }
            emit drawFrequencyDomain(frequencyData);
        }
    }
}

void Microphone::doStaticalDataProcessingForTwoSeconds(
    std::vector<float> sampledBuffer)
{
    const size_t       sampledBufferSize = sampledBuffer.size();
    std::vector<float> BHWindow;
    BHWindow.resize(sampledBufferSize);
    buildBlackmanHarrisWindow(BHWindow);
    {
        std::lock_guard<std::mutex> lock(g_mutex);
        for (int i = 0; i < sampledBuffer.size(); i++)
        {
            sampledBuffer[i] *= BHWindow[i];
        }
        // 65536 -- ближайшее значение большее 48000, которое является
        // степенью двойки
        //        sampledBuffer.resize(65536);

        //            std::cout << "start fourier transfrom" << std::endl <<
        //            std::flush;
        QFourierTransformer transformer;
        // Setting a fixed size for the transformation
        if (transformer.setSize(131072) == QFourierTransformer::VariableSize)
        {
            //                std::cout
            //                    << "This size is not a default fixed size
            //                    of QRealFourier. "
            //                       "Using a variable size instead."
            //                    << std::endl
            //                    << std::flush;
        }
        else if (transformer.setSize(131072) ==
                 QFourierTransformer::InvalidSize)
        {
            std::cout << "Invalid FFT size." << std::endl << std::flush;
            return;
        }
        std::vector<float> signalForTwoSecondInFreaqunceRange;
        signalForTwoSecondInFreaqunceRange.resize(131072);
        transformer.forwardTransform(
            const_cast<float*>(sampledBuffer.data()),
            const_cast<float*>(signalForTwoSecondInFreaqunceRange.data()));
        //            std::cout << "fourier transforming finished." <<
        //            std::endl
        //                      << std::flush;
        QComplexVector complexVectorValues =
            transformer.toComplex(signalForTwoSecondInFreaqunceRange.data());
        //            std::cout
        //                << "complex transforming finished, start transform
        //                data fo"
        //                   "drawning "
        //                << std::endl
        //                << std::flush;

        QVector<QCPGraphData> frequencyData(131072 / 2.0);
        float                 step = 24000.0 / 65536.0;
        for (size_t i = 0; i < 131072 / 2.0; i++)
        {
            frequencyData[i].key = i * step;
            float real           = complexVectorValues[i].real();
            float imaginary      = complexVectorValues[i].imaginary();
            float amplitude = std::sqrt(real * real + imaginary * imaginary);
            qreal valueIndB = QAudio::convertVolume(amplitude,
                                                    QAudio::LinearVolumeScale,
                                                    QAudio::DecibelVolumeScale);
            frequencyData[i].value = valueIndB;
        }
        emit drawFrequencyDomainForTwoSeconds(frequencyData);
    }
}

void Microphone::initAudioInfo()
{
    switch (m_format.sampleSize())
    {
        case 8:
            switch (m_format.sampleType())
            {
                case QAudioFormat::UnSignedInt:
                    m_maxAmplitude = 255;
                    break;
                case QAudioFormat::SignedInt:
                    m_maxAmplitude = 127;
                    break;
                default:
                    break;
            }
            break;
        case 16:
            switch (m_format.sampleType())
            {
                case QAudioFormat::UnSignedInt:
                    m_maxAmplitude = 65535;
                    break;
                case QAudioFormat::SignedInt:
                    m_maxAmplitude = 32767;
                    break;
                default:
                    break;
            }
            break;

        case 32:
            switch (m_format.sampleType())
            {
                case QAudioFormat::UnSignedInt:
                    m_maxAmplitude = 0xffffffff;
                    break;
                case QAudioFormat::SignedInt:
                    m_maxAmplitude = 0x7fffffff;
                    break;
                case QAudioFormat::Float:
                    m_maxAmplitude = 0x7fffffff; // Kind of
                default:
                    break;
            }
            break;

        default:
            break;
    }
}

bool Microphone::buildBlackmanHarrisWindow(std::vector<float>& window)
{
    const float a0 = 0.35875f;
    const float a1 = 0.48829f;
    const float a2 = 0.14128f;
    const float a3 = 0.01168f;

    unsigned int idx = 0;
    size_t       num = window.size();
    while (idx < num)
    {
        window[idx] = a0 - (a1 * cosf((2.0f * M_PI * idx) / (num - 1))) +
                      (a2 * cosf((4.0f * M_PI * idx) / (num - 1))) -
                      (a3 * cosf((6.0f * M_PI * idx) / (num - 1)));
        idx++;
    }
    return true;
}
