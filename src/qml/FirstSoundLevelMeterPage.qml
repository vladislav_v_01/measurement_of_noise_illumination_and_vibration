import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtCharts 2.15

Item{
    id: root;

    property real angle: -44;

    SequentialAnimation on angle{
        loops: Animation.Infinite
        NumberAnimation { to : 226; duration: 5000; easing.type: Easing.InOutCubic }
        NumberAnimation { to : -44; duration: 5000; easing.type: Easing.InOutCubic }
    }

    GridLayout{

        rows: 5;
        columns: 1;

        anchors.fill: parent;

        GridLayout{
            rows:    1;
            columns: 3;

            Layout.row:        0;
            Layout.column:     0;
            Layout.fillWidth:  true;
            Layout.alignment:  Qt.AlignHCenter;

            FontAwesomeButton{
                id: resetValuesLabel;

                Layout.row:        0;
                Layout.column:     0;
                Layout.alignment:  Qt.AlignBottom | Qt.AlignRight;
                Layout.leftMargin: root.width / 20.0;
                Layout.fillWidth: true;
                Layout.maximumWidth: incrGraphResLabel.width;

                text:          "\uF01E";
                textPointSize: 30;
                textPadding:   root.height / 70.0;
            }

            Image {
                id: vibrometer

                source:       "qrc:/images/speedometer.png";
                fillMode:     Image.PreserveAspectFit/*PreserveAspectCrop*/;
                antialiasing: true;

                Layout.row:           0;
                Layout.column:        1;
                Layout.fillWidth:     true;
                Layout.fillHeight:    true;
                Layout.maximumHeight: root.height / 3.0;
                Layout.alignment:     Qt.AlignVCenter | Qt.AlignHCenter;
                Layout.topMargin:     root.height / 20.0;
            }

            Image {
                id: vibrometerArrow;

                source:       Material.theme == Material.Dark ? "qrc:/images/arrow.png" : "qrc:/images/vibrometer_arrow.png";
                fillMode:     Image.PreserveAspectFit;
                antialiasing: true;
                transformOrigin: Item.Center;
                scale:        vibrometer.paintedWidth / vibrometerArrow.paintedWidth / 2.0;

                Layout.row:           0;
                Layout.column:        1;
                Layout.fillHeight:    true;
                Layout.fillWidth:     true;
                Layout.maximumHeight: root.height / 3.0;
                Layout.alignment:     Qt.AlignHCenter | Qt.AlignVCenter;
                Layout.topMargin:     root.height / 20.0;

                transform: [
                    Translate {x: -vibrometerArrow.width / 2.45 * vibrometerArrow.scale;
                               y: (vibrometerArrow.implicitHeight / 1.72 - vibrometerArrow.implicitHeight / 2.0) * vibrometerArrow.scale},
                    Rotation  {origin.x: vibrometer.width / 2.0;
                               origin.y: vibrometer.height / 2.0;
                               angle: root.angle;}
                ]
            }

            FontAwesomeButton{
                id: switchBetweenMMIAndGraphLabel;

                Layout.row:         0;
                Layout.column:      2;
                Layout.alignment:   Qt.AlignBottom | Qt.AlignLeft;
                Layout.rightMargin: root.width / 20.0;
                Layout.fillWidth: true;
                Layout.maximumWidth: incrGraphResLabel.width;

                text:          "\uF15C";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                onPressed: {
                    switchBetweenMMIAndGraphLabel.text = switchBetweenMMIAndGraphLabel.text == "\uF15C" ? "\uF080" : "\uF15C";
                    decibelLevelsTabel.visible = switchBetweenMMIAndGraphLabel.text == "\uF15C" ? false : true;
                }
            }
        }

        GridLayout{

            rows:    2;
            columns: 5;

            Layout.row:         1;
            Layout.column:      0;
            Layout.alignment:   Qt.AlignHCenter;
            Layout.leftMargin:  root.width / 20.0;
            Layout.rightMargin: root.width / 20.0;

            TextLabel{
                id: minTextLabel;

                Layout.row:       0;
                Layout.column:    0;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;

                text: qsTr("min");
            }

            Rectangle{
                Layout.row:       0;
                Layout.column:    1;
                Layout.fillWidth: true;
            }

            TextLabel{
                id: avgTextLabel;

                Layout.row:       0;
                Layout.column:    2;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;

                text: qsTr("avg");
            }

            Rectangle{
                Layout.row:       0;
                Layout.column:    3;
                Layout.fillWidth: true;
            }

            TextLabel{
                id: maxTextLabel;

                Layout.row:       0;
                Layout.column:    4;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;

                text: qsTr("max");
            }

            ValueLabel{
                id: minValueLabel;
                objectName: qsTr("minValueLabel");

                Layout.row:       1;
                Layout.column:    0;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;
            }

            Rectangle{
                Layout.row:       1;
                Layout.column:    1;
                Layout.fillWidth: true;
            }

            ValueLabel{
                id: avgValueLabel;
                objectName: qsTr("avgValueLabel");

                Layout.row:       1;
                Layout.column:    2;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;
            }

            Rectangle{
                Layout.row:       1;
                Layout.column:    3;
                Layout.fillWidth: true;
            }

            ValueLabel{
                id: maxValueLabel;
                objectName: qsTr("maxValueLabel");

                Layout.row:       1;
                Layout.column:    4;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;
            }
        }

        Label{
            id: currDecibelLevelOfSoundLabel;

            Layout.row:       2;
            Layout.column:    0;
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;

            text:           qsTr("Current decibel level of sound");
            color:          Material.accent;
            font.pointSize: 20;
            font.bold:      true;

            visible: !decibelLevelsTabel.visible;
        }

        RowLayout{

            Layout.row:         3;
            Layout.column:      0;
            Layout.fillWidth:   true;
            Layout.leftMargin:  root.width / 20.0;
            Layout.rightMargin: root.width / 20.0;

            FontAwesomeButton{
                id: incrGraphResLabel;

                text:          "\uF00E";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                visible: !decibelLevelsTabel.visible;
            }

            FontAwesomeButton{
                id: reduceGraphResLabel;

                text:          "\uF010";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                visible: !decibelLevelsTabel.visible;
            }

            Rectangle{
                Layout.fillWidth: true;

                visible: !decibelLevelsTabel.visible;
            }

            FontAwesomeButton{
                id: stopStartLabel;

                Layout.fillWidth: true;
                Layout.maximumWidth: incrGraphResLabel.width;

                text:          "\uF04C";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                visible: !decibelLevelsTabel.visible;

                onPressed: {
                    stopStartLabel.text = stopStartLabel.text == "\uF04C" ? "\uF04B" : "\uF04C";
                }
            }
        }

        ChartView {
            id: rawDataChart;

            Layout.row:          4;
            Layout.column:       0;
            Layout.fillWidth:    true;
            Layout.fillHeight:   true;
            Layout.alignment:    Qt.AlignTop;
            Layout.leftMargin:   root.width / 40.0;
            Layout.rightMargin:  root.width / 40.0;
            Layout.topMargin:    root.width / 40.0;
            Layout.bottomMargin: root.width / 40.0;

            legend.visible: false

            backgroundColor: Material.background;

            ValueAxis {
                id: xAxis
                min: 0
                max: 2000

                labelsColor: Material.foreground;
            }

            ValueAxis {
                id: yAxis
                min: -1
                max:  1

                labelsColor: Material.foreground;
            }


            LineSeries {
                id: series;

                axisX: xAxis;
                axisY: yAxis;

                color: "red";
                width: 3;
            }

            onSeriesAdded: {
                noiseMeterWorker.series = rawDataChart.series(0);
                noiseMeterWorker.onNewSeries();
            }
        }


//        CustomPlot{
//            id: someTimeElapsedCustomPlot;

//            Layout.row:          4;
//            Layout.column:       0;
//            Layout.fillWidth:    true;
//            Layout.fillHeight:   true;
//            Layout.alignment:    Qt.AlignTop;
//            Layout.leftMargin:   root.width / 40.0;
//            Layout.rightMargin:  root.width / 40.0;
//            Layout.topMargin:    root.width / 40.0;
//            Layout.bottomMargin: root.width / 40.0;

//            visible: !decibelLevelsTabel.visible;
//        }

        ListView{
            id: decibelLevelsTabel;

            Layout.row:          3;
            Layout.column:       0;
            Layout.fillWidth:    true;
            Layout.fillHeight:   true;
            Layout.alignment:    Qt.AlignTop;
            Layout.leftMargin:   root.width / 6.0;
            Layout.rightMargin:  root.width / 40.0;
            Layout.bottomMargin: root.width / 40.0;
            Layout.topMargin:    root.width / 40.0;

            visible: false;
            ScrollBar.vertical: ScrollBar {
                hoverEnabled: true;
                active: hovered || enabled;
            }

            model: DecibelLevelsOfSound{}
            delegate: Text {
                // @disable-check M325
                text:                qsTr((db_value == "0dB" ? pointer + " " + db_value : db_value) + ": " + description);
                font.pointSize:      18;
                // @disable-check M325
                color:               db_value == "0dB" ? "red" : Material.accent;
                horizontalAlignment: Text.AlignLeft;
            }
        }
    }


    Component.onCompleted: {
        noiseMeterWorker.onItemCompleted(minValueLabel);
        noiseMeterWorker.onItemCompleted(avgValueLabel);
        noiseMeterWorker.onItemCompleted(maxValueLabel);
    }
}
