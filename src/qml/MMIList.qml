import QtQuick 2.15

ListModel{
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u216B";
        description: qsTr("Cataclysmic. Total destruction.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u216A";
        description: qsTr("Extreme. Rails bent greatly.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2169";
        description: qsTr("Intense. Almost destroyed.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2168";
        description: qsTr("Violent. Noticeable ground cracks.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2167";
        description: qsTr("Destructive. Fall of walls.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2166";
        description: qsTr("Very strong. Difficult to stand.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2165";
        description: qsTr("Strong. Heavy furniture moved.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2164";
        description: qsTr("Rather strong. Dishes broken.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2163";
        description: qsTr("Moderate. Hanging objects swing.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2162";
        description: qsTr("Slight. Felt indoors by several.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2161";
        description: qsTr("Weak. Felt indoors by several.");
    }
    ListElement{
        pointer:     "\uF0DA";
        scale_level: "\u2160";
        description: qsTr("Instrumental. Felt by animals.");
    }
}
