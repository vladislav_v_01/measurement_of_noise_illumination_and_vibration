import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

Label{
    id: root;

    color: Material.accent;
    font.pointSize: 30;
    font.bold: true;
    font.family: qsTr("DS-Digital");
}

