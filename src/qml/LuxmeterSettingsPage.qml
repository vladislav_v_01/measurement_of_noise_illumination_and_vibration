import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

Page {
    id: root;
    objectName: qsTr("Luxmeter settings");

    RowLayout {
        anchors.fill: parent;
        anchors.margins: 20 * heightRatio;

        spacing: 30;

        Label {
            id: rippleFactorMeasuringDurationLabel;

            Layout.fillWidth: true;
            Layout.alignment: Qt.AlignTop;

            text: "Ripple factor measuring duraction";
            color: Material.accent;
            font.pointSize: 20 * heightRatio;

            wrapMode: Text.WordWrap;
        }

        CustomizedComboBox {
            id: rippleFactorMeasuringDuration;

            Layout.fillWidth: true;
            Layout.alignment: Qt.AlignTop;

            font.pointSize: 20 * heightRatio;

            model: ["5 s", "10 s", "20 s", "30 s",
                    "40 s", "50 s", "60 s"]
        }
    }
}
