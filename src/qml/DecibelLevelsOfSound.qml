import QtQuick 2.15

ListModel{
    ListElement{
        pointer:  "\uF0DA";
        db_value: "180dB";
        description: qsTr("Rocket launch platform.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "170dB";
        description: qsTr("Avalanche firework.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "160dB";
        description: qsTr("Shooting with pistol or rifle.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "150dB";
        description: qsTr("Fireworks.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "140dB";
        description: qsTr("Pain threshold.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "130dB";
        description: qsTr("Thunder.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "120dB";
        description: qsTr("Police siren.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "110dB";
        description: qsTr("Rock concert.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "100dB";
        description: qsTr("Fighter jet at 300m.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "90dB";
        description: qsTr("Shouting.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "80dB";
        description: qsTr("Alarm clock.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "70dB";
        description: qsTr("Television set on loud.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "60dB";
        description: qsTr("Sound of human voice.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "50dB";
        description: qsTr("Refrigerator working.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "40dB";
        description: qsTr("Living room.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "30dB";
        description: qsTr("Whispering.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "20dB";
        description: qsTr("Rustles of autumnal leaves.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "10dB";
        description: qsTr("A leaf falling.");
    }
    ListElement{
        pointer:  "\uF0DA";
        db_value: "0dB";
        description: qsTr("Complete silence.");
    }
}
