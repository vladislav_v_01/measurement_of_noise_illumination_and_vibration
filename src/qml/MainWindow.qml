import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.15

import "."

import "./FontAwesome"

ApplicationWindow {
    id: window;
    minimumWidth: 360;
    minimumHeight: 520;
    visible: true;
    title: qsTr("Pumu");

    Material.theme: Material.Dark;

    readonly property real heightRatio: window.height / 1920.0;
    readonly property real widthRatio: window.width / 1080.0;

    FontAwesome {
        id: awesome;

        resource: "qrc:/fonts/Font Awesome 5 Free-Solid-900.otf";
    }

    FontLoader {
        id: dsDigitalBold;

        source: "qrc:/fonts/DS-Digital-Bold.ttf";
    }

    Shortcut{
        sequences: ["Esc", "Ctrl+q"];
        onActivated: {
            close();
        }
        context: Qt.ApplicationShortcut;
    }

    header: ToolBar{
        Material.elevation: 5;

        RowLayout {
            id: toolBarLayout;
            spacing: 20 * heightRatio;

            anchors.fill: parent

            ToolButton{
                id: exitButton;

                Layout.alignment: Qt.AlignLeft;

                text: awesome.loaded ? ( stackView.depth > 1 ?
                                            awesome.icons.chevronCircleLeft :
                                            awesome.icons.timeCircle ) : "";
                font.pointSize: 45 * heightRatio;

                onClicked: stackView.depth > 1 ? stackView.pop() : close();
            }

            Label {
                id: pageNameLabel;

                Layout.alignment: Qt.AlignHCenter;

                text: stackView.width > 1 ? stackView.currentItem.objectName : "";
                font.pointSize: 30 * heightRatio;
                color: Material.accent;

            }

            ToolButton {
                Layout.alignment: Qt.AlignRight;
                text: awesome.loaded ? awesome.icons.infoCircle : "" /*"\uF05A"*/;
                font.pointSize: 45 * heightRatio;

                onClicked: optionsMenu.open();

                Menu {
                    id: optionsMenu;
                    transformOrigin: Menu.TopRight;
                    font.pointSize: 30 * heightRatio;
                    margins: {
                        leftMargin = 10 * widthRatio;
                        rightMargin = 10 * widthRatio;
                        topMargin = 10 * heightRatio;
                        bottomMargin = 10 * heightRatio;
                    }

                    Action {
                        text: "Settings";
                        onTriggered: {

                            if( stackView.currentItem.objectName === "Luxmeter" ) {
                                stackView.push(luxmeterSettingsPage);
                            } else {
                                stackView.push(commonSettingsPage);
                            }
                        }
                    }

                    Component{
                        id: commonSettingsPage;

                        CommonSettingsPage{}
                    }

                    Component{
                        id: luxmeterSettingsPage;

                        LuxmeterSettingsPage{}
                    }

                    Action {
                        text: "Help";
                        onTriggered: helpDialog.open();
                    }
                    Action {
                        text: "About";
                        onTriggered: aboutDialog.open();
                    }

                    background: Rectangle {
                            implicitWidth: window.width / 2.6 * widthRatio;
                            color: Material.background;
                        }
                }
            }
        }
    }

    StackView{
        id: stackView;
        anchors.fill: parent;

        initialItem: modeSelectionWindow;
        Component{
            id: modeSelectionWindow;

            ModeSelectionWindow{}
        }
    }


}
