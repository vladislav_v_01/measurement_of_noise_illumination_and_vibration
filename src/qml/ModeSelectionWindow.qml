import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "."

Item {
    id: root;

    ColumnLayout{
        anchors.fill: parent;
        anchors.leftMargin: parent.width / 20.0 * widthRatio;
        anchors.rightMargin: parent.width / 20.0 * widthRatio;

        spacing: 10;

        ModeButton{
            id: soundLevelMeter;

            Layout.fillWidth: true;
            Layout.alignment: Qt.AlignBottom;
            Layout.bottomMargin: parent.height / 15.0 * heightRatio;

//            anchors.bottom: luxmeter.top;
//            anchors.bottomMargin: parent.height / 15.0 * heightRatio;
            implicitHeight: parent.height / 7.0 * heightRatio;

            text: qsTr("Sound level meter");

            backgroundColor: Material.accent;
            textPointSize: 30 * heightRatio;

            onClicked: {
                stackView.push(["qrc:/qml/SoundLevelMeterWindow.qml"]);
            }
        }

        ModeButton{
            id: luxmeter;

            Layout.fillWidth: true;
            Layout.alignment: Qt.AlignHCenter;

            implicitHeight: parent.height / 7.0 * heightRatio;

            text: qsTr("Luxmeter");

            backgroundColor: Material.accent;
            textPointSize: 30 * heightRatio;

            onClicked: {
                stackView.push(luxmeterPage);
            }

            LuxmeterWindow{
                id:         luxmeterPage;
                objectName: "Luxmeter";

                visible: false;
            }
        }

        ModeButton{
            id: vibrometer;

            Layout.fillWidth: true;
            Layout.alignment: Qt.AlignTop;
            Layout.topMargin: parent.height / 15.0 * heightRatio;

//            anchors.top: luxmeter.bottom;
//            anchors.topMargin: parent.height / 15.0 * heightRatio;
            implicitHeight: parent.height / 7.0 * heightRatio;

            backgroundColor: Material.accent;

            text: qsTr("Vibrometer");
            textPointSize: 30 * heightRatio;

            onClicked: {
                stackView.push(["qrc:/qml/VibrometerWindow.qml"]);
            }
        }
    }
}
