import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

import "."

Button {
    id: root

    property int   textPointSize:             20;
    property int   textPadding:               10;
    property color fontAwesomeAnimationColor: "#77A7C268";

    signal pressed;
    signal released;

    contentItem: Text{
        id: buttonText;
        objectName: qsTr("flickableButtonText");

        text:                root.text;
        font.pointSize:      root.textPointSize;
        color:               Material.accent;
        horizontalAlignment: Text.AlignHCenter;
        verticalAlignment:   Text.AlignVCenter;
        elide:               Text.ElideRight;
        padding:             root.textPadding;
    }

    background: Rectangle{
        id: backgroundRectangle;

        color:        Material.background;
        border.color: Material.accent;
        border.width: 5;
        radius:       15;
        clip:         true;

        Rectangle {
            id: colorRect;

            height:       0;
            width:        0;
            color:        fontAwesomeAnimationColor;
            border.color: fontAwesomeAnimationColor;
            border.width: 5;
            radius:       15;
            clip:         true;

            transform: Translate {
                x: -colorRect.width / 2.0;
                y: -colorRect.height / 2.0;
            }
        }
    }

    MouseArea{
        id: mouseArea;

        anchors.fill: parent;

        hoverEnabled: true;

        onPressed:  {
            colorRect.x = mouseX;
            colorRect.y = mouseY;
            circleAnimation.start();
            root.pressed();
        }
        onReleased: {
            circleAnimation.stop();
            backgroundRectangle.color = Material.background;
            root.released();
        }
    }

    PropertyAnimation {
        id: circleAnimation;

        target:     colorRect;
        properties: "width,height,radius";
        from:       backgroundRectangle.width / 1.5;
        to:         backgroundRectangle.width * 3.0;
        duration:   500;

        onFinished: {
            backgroundRectangle.color =  fontAwesomeAnimationColor;
        }

        onStopped: {
            backgroundRectangle.color = Material.background;
            colorRect.width           = 0;
            colorRect.height          = 0;
        }
    }
}

