import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

import "."

Item {
    id: root;

    GridLayout{

        rows:    5;
        columns: 1;

        anchors.fill: parent;

        Image{
            id: vibrometerBorder;

            source: "qrc:/images/vibrometer_background_border.png"

            z: 2;

            Layout.row:           0;
            Layout.column:        0;
            Layout.fillWidth:     true;
            Layout.fillHeight:    true;
            Layout.maximumHeight: root.height / 4.0;
            Layout.leftMargin:    root.width  / 40.0;
            Layout.rightMargin:   root.width  / 40.0;
            Layout.topMargin:     root.height / 40.0;
            Layout.bottomMargin:  root.height / 40.0;
        }

        CustomPlot{
            id: vibrometerRealTimeCustomPlot;

            z: 0;

            Layout.row:           0;
            Layout.column:        0;
            Layout.fillWidth:     true;
            Layout.fillHeight:    true;
            Layout.maximumHeight: root.height / 4.0;
            Layout.leftMargin:    root.width  / 40.0;
            Layout.rightMargin:   root.width  / 40.0;
            Layout.topMargin:     root.height / 40.0;
            Layout.bottomMargin:  root.height / 40.0;

            // вернуть указатель по созданию, чтобы убрать значения у осей
            // и настроить линии на графике + проверить при этом будет
            // ли корректно отображаться стрелка
        }

        Image {
            id: vibrometerArrow;

            source:       Material.theme == Material.Dark ? "qrc:/images/arrow.png" : "qrc:/images/vibrometer_arrow.png";
            fillMode:     Image.PreserveAspectFit;
            antialiasing: true;

            z:               1;
            scale:           0.5;

            Layout.row:           0;
            Layout.column:        0;
            Layout.fillHeight:    true;
            Layout.alignment:     Qt.AlignRight | Qt.AlignVCenter;
            Layout.maximumHeight: root.height / 4.0;
            Layout.topMargin:     root.height / 40.0;
            Layout.bottomMargin:  root.height / 40.0;
        }

        GridLayout{

            rows:    2;
            columns: 5;

            Layout.row:             1;
            Layout.column:          0;
            Layout.alignment:       Qt.AlignHCenter;
            Layout.leftMargin:      root.width / 20.0;
            Layout.rightMargin:     root.width / 20.0;

            TextLabel{
                id: minTextLabel;

                Layout.row:       0;
                Layout.column:    0;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;

                text: qsTr("min");
            }

            Rectangle{
                Layout.row:       0;
                Layout.column:      1;
                Layout.fillWidth: true;
            }

            TextLabel{
                id: avgTextLabel;

                Layout.row:       0;
                Layout.column:    2;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;

                text: qsTr("avg");
            }

            Rectangle{
                Layout.row:       0;
                Layout.column:    3;
                Layout.fillWidth: true;
            }

            TextLabel{
                id: maxTextLabel;

                Layout.row:       0;
                Layout.column:    4;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;

                text: qsTr("max");
            }

            ValueLabel{
                id: minValueLabel;

                Layout.row:       1;
                Layout.column:    0;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;
            }

            Rectangle{
                Layout.row:       1;
                Layout.column:    1;
                Layout.fillWidth: true;
            }

            ValueLabel{
                id: avgValueLabel;

                Layout.row:       1;
                Layout.column:    2;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;
            }

            Rectangle{
                Layout.row:       1;
                Layout.column:    3;
                Layout.fillWidth: true;
            }

            ValueLabel{
                id: maxValueLabel;

                Layout.row:       1;
                Layout.column:    4;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;
            }
        }

        Label{
            id: currMMIValueLabel;

            Layout.row:       2;
            Layout.column:    0;
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;

            text:           qsTr("MMI current value");
            color:          Material.accent;
            font.pointSize: 30;
            font.bold:      true;

            visible: !mmiTabel.visible;
        }

        RowLayout{

            Layout.row:         3;
            Layout.column:      0;
            Layout.fillWidth:   true;
            Layout.leftMargin:  root.width / 20.0;
            Layout.rightMargin: root.width / 20.0;

            FontAwesomeButton{
                id: incrGraphResLabel;

                text:          "\uF00E"/* "\uF067"*/;
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                visible: !mmiTabel.visible;
            }

            FontAwesomeButton{
                id: reduceGraphResLabel;

                text:          "\uF010" /*"\uF068"*/;
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                visible: !mmiTabel.visible;
            }

            Rectangle{
                Layout.fillWidth: !mmiTabel.visible;
            }

            FontAwesomeButton{
                id: switchBetweenMMIAndGraphLabel;

                Layout.fillWidth: true;
                Layout.maximumWidth: mmiTabel.visible ? root.width : incrGraphResLabel.width ;

                text:          "\uF15C";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                onPressed: {
                    switchBetweenMMIAndGraphLabel.text = switchBetweenMMIAndGraphLabel.text == "\uF15C" ? "\uF080" : "\uF15C";
                    mmiTabel.visible = switchBetweenMMIAndGraphLabel.text == "\uF15C" ? false : true;
                }
            }

            FontAwesomeButton{
                id: resetValuesLabel;

                Layout.fillWidth: true;
                Layout.maximumWidth: mmiTabel.visible ? root.width : incrGraphResLabel.width;

                text:          "\uF01E";
                textPointSize: 30;
                textPadding:   root.height / 70.0;
            }

            FontAwesomeButton{
                id: stopStartLabel

                Layout.fillWidth: true;
                Layout.maximumWidth: mmiTabel.visible ? root.width : incrGraphResLabel.width;

                text:          "\uF04C";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                onPressed: {
                    stopStartLabel.text = stopStartLabel.text == "\uF04C" ? "\uF04B" : "\uF04C";
                }
            }
        }

        CustomPlot{
            id: someTimeElapsedCustomPlot;

            Layout.row:          4;
            Layout.column:       0;
            Layout.fillWidth:    true;
            Layout.fillHeight:   true;
            Layout.alignment:    Qt.AlignTop;
            Layout.leftMargin:   root.width / 40.0;
            Layout.rightMargin:  root.width / 40.0;
            Layout.bottomMargin: root.width / 40.0;

            visible: !mmiTabel.visible;
        }

        ListView{
            id: mmiTabel;

            Layout.row:          4;
            Layout.column:       0;
            Layout.fillWidth:    true;
            Layout.fillHeight:   true;
            Layout.alignment:    Qt.AlignTop;
            Layout.leftMargin:   root.width / 10.0;
            Layout.rightMargin:  root.width / 40.0;
            Layout.bottomMargin: root.width / 40.0;

            visible: false;
            interactive: false

            model: MMIList{}
            delegate: Text {

                // @disable-check M325
                text:           qsTr((scale_level == "\u2160" ? pointer + " " + scale_level : scale_level) + ". " + description);
                font.pointSize: 18;
                // @disable-check M325
                color:          scale_level == "\u2160" ? "red" : Material.accent;
                horizontalAlignment: Text.AlignLeft;
            }
        }
    }
}
