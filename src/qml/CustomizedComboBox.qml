import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

ComboBox {
    id: control

    property color textColor: Material.accent;
    property color colorNonPressed: Material.accent;
    property color colorPressed: Material.accent;
    property color backgroundColor: Material.background;
    property color borderColor: Material.accent;

    delegate: ItemDelegate {
        width: control.width;
        contentItem: Text {
            text: modelData
            color: textColor
            font: control.font
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }
        highlighted: control.highlightedIndex === index
    }

    indicator: Canvas {
        id: canvas
        x: control.width - width - control.rightPadding
        y: control.topPadding + (control.availableHeight - height) / 2
        width: control.width * 0.1;
        height: control.height * 0.1 * 2 / 3;
        contextType: "2d"

        Connections {
            target: control
            function onPressedChanged() { canvas.requestPaint(); }
        }

        onPaint: {
            context.reset();
            context.moveTo(0, 0);
            context.lineTo(width, 0);
            context.lineTo(width / 2, height);
            context.closePath();
            context.fillStyle = control.pressed ? colorPressed : colorNonPressed;
            context.fill();
        }
    }

    contentItem: Text {
        leftPadding: 30
        topPadding: 30;
        bottomPadding: 30
        rightPadding: control.indicator.width + 0;

        text: control.displayText
        font: control.font
        color: control.pressed ? colorPressed : colorNonPressed
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitWidth: control.width * 1.35;
        color: backgroundColor
        border.color: control.pressed ? colorPressed : colorNonPressed
        border.width: control.visualFocus ? 2 : 1
        radius: 2
    }

    popup: Popup {
        y: control.height - 1
        width: control.width
        implicitHeight: contentItem.implicitHeight
        padding: 1

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: control.popup.visible ? control.delegateModel : null
            currentIndex: control.highlightedIndex

            ScrollIndicator.vertical: ScrollIndicator { }
        }

        background: Rectangle {
            color: backgroundColor;
            border.color: borderColor;
            radius: 2
        }
    }

    onActivated: {
        luxMeterWorker.onNewRFMeasuringTime(control.displayText);
    }
}
