import QtQuick 2.15
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.15

Item {
    id: root;

    ColumnLayout{
        anchors.fill: parent;

        CustomPlot{
            id: realtimeFrequencyCustomPlot;

            Layout.fillWidth:    true;
            Layout.fillHeight:   true;
            Layout.alignment:    Qt.AlignTop;
            Layout.leftMargin:   root.width / 40.0;
            Layout.rightMargin:  root.width / 40.0;
            Layout.topMargin:    root.width / 20.0;
            Layout.bottomMargin: root.width / 40.0;
        }

        ModeButton{
            id: startStopMesuring;

            Layout.fillWidth:    true;
            Layout.alignment:    Qt.AlignHCenter;
            Layout.leftMargin:   root.width / 40.0;
            Layout.rightMargin:  root.width / 40.0;
            Layout.bottomMargin: root.width / 40.0;

            implicitHeight: root.height / 7.0;

            text: qsTr("Start mesuring");

            backgroundColor: Material.accent;
            textPointSize:   30;

            onClicked: {
                // @disable-check M126
                text = text == qsTr("Start mesuring") ? qsTr("Stop mesuring") :
                                                        qsTr("Start mesuring");
            }
        }
    }
}
