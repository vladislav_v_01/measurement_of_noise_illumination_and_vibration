import QtQuick 2.15

QtObject {
    readonly property string tools:                     "\uF7D9";
    readonly property string info:                      "\uF129";
    readonly property string usersCogs:                 "\uF509";
    readonly property string chevronRight:              "\uF054";
    readonly property string chevronLeft:               "\uF053";
    readonly property string chevronCircleLeft:         "\uF137";
    readonly property string timeCircle:                "\uF057";
    readonly property string thermometerFull:           "\uF2C7";
    readonly property string thermometerThreeQuarters:  "\uF2C8";
    readonly property string thermometerHalf:           "\uF2C9";
    readonly property string thermometerEmpty:          "\uF2CB";
    readonly property string thermometerQuarter:        "\uF2CA";
    readonly property string trash:                     "\uF2ED";
    readonly property string edit:                      "\uF044";
    readonly property string plus:                      "\uF067";
    readonly property string minus:                     "\uF068";
    readonly property string searchPlus:                "\uF00E";
    readonly property string searchMinus:               "\uF010";
    readonly property string verticalElipsis:           "\uF142";
    readonly property string infoCircle:                "\uF05A";
}
