import QtQuick 2.15

import "impl" as Awesome

Item {
    id: awesome;

    property alias icons: variables;
    property alias loaded: loader.loaded;
    property alias resource: loader.resource;

    readonly property string family: "Font Awesome 5 Free"

    Awesome.Loader {
        id:loader;
    }



    Awesome.Variables {
        id: variables;
    }
}
