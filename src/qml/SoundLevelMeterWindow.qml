import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

import "."

Item {
    id: root;

    SwipeView{
        id: view;

        currentIndex: 0;
        anchors.fill: parent;

        FirstSoundLevelMeterPage{
            id: firstPage
        }

        SecondSoundLevelMeterPage{
            id: secondPage;
        }
    }

    PageIndicator {
        id: indicator

        count:        view.count
        currentIndex: view.currentIndex

        anchors.top:              view.top;
        anchors.topMargin:        root.height / 80.0;
        anchors.horizontalCenter: parent.horizontalCenter

        delegate: Rectangle {
                implicitWidth:  root.height / 80.0;
                implicitHeight: root.height / 80.0;


                radius: width / 2.0
                color:  Material.accent;

                // @disable-check M325
                opacity: index === indicator.currentIndex ? 0.95 : pressed ? 0.7 : 0.45

                Behavior on opacity {
                    OpacityAnimator {
                        duration: 100
                    }
                }
            }
    }

}
