import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtGraphicalEffects 1.15

Item {
    id: root;

    GridLayout{
        rows:    3;
        columns: 1;

        anchors.fill: parent;

        Image {
            id: luxmeterBackground;

            z: -1;

            source: "qrc:/images/monotone_background_2.jpg"

            Layout.row:          0;
            Layout.column:       0;
            Layout.fillWidth:    true;
            Layout.fillHeight:   true;
            Layout.alignment:    Qt.AlignHCenter;
            Layout.leftMargin:   root.width  / 20.0;
            Layout.rightMargin:  root.width  / 20.0;
            Layout.topMargin:    root.height / 40.0;
            Layout.bottomMargin: root.height / 40.0;
        }

        Image {
            id: luxmeterBackgroundBorder;

            source: "qrc:/images/backgound_border.png"

            Layout.row:          0;
            Layout.column:       0;
            Layout.fillWidth:    true;
            Layout.fillHeight:   true;
            Layout.alignment:    Qt.AlignHCenter;
            Layout.leftMargin:   root.width  / 20.0;
            Layout.rightMargin:  root.width  / 20.0;
            Layout.topMargin:    root.height / 40.0;
            Layout.bottomMargin: root.height / 40.0;
        }

        GridLayout{
            rows:    3;
            columns: 3;

            Layout.row:             0;
            Layout.column:          0;
            Layout.alignment:       Qt.AlignHCenter;
            Layout.leftMargin:      root.width  / 20.0;
            Layout.rightMargin:     root.width  / 20.0;
            Layout.topMargin:       root.height / 20.0;
            Layout.bottomMargin:    root.height / 20.0;


            TextLabel{
                id: minTextLabel;

                Layout.row:       0;
                Layout.column:    0;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;

                text: qsTr("min");
            }

            TextLabel{
                id: avgTextLabel;

                Layout.row:       0;
                Layout.column:    1;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;

                text: qsTr("avg");
            }

            TextLabel{
                id: maxTextLabel;

                Layout.row:       0;
                Layout.column:    2;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;

                text: qsTr("max");
            }

            ValueLabel{
                id: minValueLabel;
                objectName:  qsTr("minValueLabel");

                Layout.row:       1;
                Layout.column:    0;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;
            }

            ValueLabel{
                id: avgValueLabel;
                objectName: qsTr("avgValueLabel");

                Layout.row:         1;
                Layout.column:      1;
                Layout.alignment:   Qt.AlignHCenter | Qt.AlignTop;
            }

            ValueLabel{
                id: maxValueLabel;
                objectName: qsTr("maxValueLabel");

                Layout.row:       1;
                Layout.column:    2;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop;
            }

            Rectangle{
                Layout.row:       2;
                Layout.column:    0;
                Layout.alignment: Qt.AlignHCenter;
            }

            Label{
                id: currValueLabel;
                objectName: qsTr("currValueLabel");

                Layout.row:          2;
                Layout.column:       1;
                Layout.alignment:    Qt.AlignHCenter | Qt.AlignVCenter;

                text:           qsTr("0000");
                color:          Material.accent;
                font.pointSize: 50;
                font.bold:      true;
                font.family:    qsTr("DS-Digital");
            }

            Label{
                id: currValueUnit;

                Layout.row:          2;
                Layout.column:       2;
                Layout.alignment:    Qt.AlignLeft | Qt.AlignVCenter;

                text:           qsTr("Lx");
                color:          Material.accent;
                font.pointSize: 30;
                font.bold:      true;
                font.family:    qsTr("DS-Digital");
            }

            Rectangle{
                Layout.row:       3;
                Layout.column:    0;
                Layout.alignment: Qt.AlignHCenter;
            }

            Label{
                id: rippleFactorValueLabel;
                objectName: qsTr("rippleFactorValueLabel");

                Layout.row:          3;
                Layout.column:       1;
                Layout.alignment:    Qt.AlignHCenter | Qt.AlignVCenter;

                text:           qsTr("000");
                color:          Material.accent;
                font.pointSize: 50;
                font.bold:      true;
                font.family:    qsTr("DS-Digital");
            }

            Label{
                id: rippleFactorValueUnit;

                Layout.row:          3;
                Layout.column:       2;
                Layout.alignment:    Qt.AlignLeft | Qt.AlignVCenter;

                text:           qsTr("% (Rf)");
                color:          Material.accent;
                font.pointSize: 30;
                font.bold:      true;
                font.family:    qsTr("DS-Digital");
            }
        }

        RowLayout{
            Layout.row:         1;
            Layout.column:      0;
            Layout.fillWidth:   true;
            Layout.leftMargin:  root.width  / 20.0;
            Layout.rightMargin: root.width  / 20.0;
            Layout.topMargin:   root.height / 40.0;

            FontAwesomeButton{
                id: reduceGraphResLabel

                text:          "\uF00E";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                onPressed: {
                    luxMeterWorker.onReduceButtonClicked();
                }
            }

            FontAwesomeButton{
                id: incrGraphResLabel;

                text:          "\uF010";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                onPressed: {
                    luxMeterWorker.onIncreaseButtonClicked();
                }
            }

            Rectangle{
                Layout.fillWidth: true;

            }

            FontAwesomeButton{
                id: resetValuesLabel;

                Layout.fillWidth: true;
                Layout.maximumWidth: incrGraphResLabel.width;

                text:          "\uF01E";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                onPressed: {
                    luxMeterWorker.onResetButtonClicked();
                }
            }

            FontAwesomeButton{
                id: startStopButton;
                objectName: qsTr("startStopButton");

                Layout.fillWidth: true;
                Layout.maximumWidth: incrGraphResLabel.width;

                text:          "\uF04C";
                textPointSize: 30;
                textPadding:   root.height / 70.0;

                onPressed: {
                    luxMeterWorker.onStopStartButtonClicked()
//                    startStopButton.text = startStopButton.text == "\uF04C" ? "\uF04B" : "\uF04C";
                }
            }
        }

        CustomPlot{
            id: luxmeterPlot;
            objectName: qsTr("luxmeterPlot");

            Layout.row:          2;
            Layout.column:       0;
            Layout.fillWidth:    true;
            Layout.fillHeight:   true;
            Layout.alignment:    Qt.AlignBottom;
            Layout.leftMargin:   root.width  / 40.0;
            Layout.rightMargin:  root.width  / 40.0;
            Layout.bottomMargin: root.height / 40.0;
        }
    }

    Component.onCompleted: {
        luxMeterWorker.onItemCompleted(minValueLabel);
        luxMeterWorker.onItemCompleted(avgValueLabel);
        luxMeterWorker.onItemCompleted(maxValueLabel);
        luxMeterWorker.onItemCompleted(currValueLabel);
        luxMeterWorker.onItemCompleted(rippleFactorValueLabel);
        luxMeterWorker.onItemCompleted(luxmeterPlot);
        luxMeterWorker.onItemCompleted(startStopButton);
    }
}
