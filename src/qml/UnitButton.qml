import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtGraphicalEffects 1.15

Button {
    id: button;

    property string unit: "lux";
    property color borderColor: "white"
    property int borderWidth: 0
    property int borderRadius: 75;
    property int textPointSize: 20
    property bool boldText: false;

    signal clicked;
    signal pressed;
    signal released;

    contentItem:  Text {
        id: buttonText

        text: button.text;
        font.pointSize: textPointSize;
        font.bold: boldText;

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    LinearGradient  {
        anchors.fill: buttonText
        source: buttonText
        gradient: Gradient {
            orientation: Gradient.Horizontal;
            GradientStop { position: 0.0; color:  button.unit === "lux" ? Material.foreground : Material.accent }
            GradientStop { position: 0.50; color: button.unit === "lux" ? Material.foreground : Material.accent }
            GradientStop { position: 0.501; color: button.unit === "lux" ? Material.accent : Material.foreground }
            GradientStop { position: 1.0; color: button.unit === "lux" ? Material.accent : Material.foreground }
        }
    }

    background: Rectangle{
        id: buttonBackground;

        gradient: Gradient{
            orientation: Gradient.Horizontal;
            GradientStop {position: 0.0; color: button.unit === "lux" ? Material.accent : Material.background}
            GradientStop {position: 0.50; color: button.unit === "lux" ? Material.accent : Material.background}
            GradientStop {position: 0.501; color: button.unit === "lux" ? Material.background : Material.accent}
            GradientStop {position: 1.0; color: button.unit === "lux" ? Material.background : Material.accent}
        }
        border.color: borderColor;
        border.width: borderWidth;
        radius: borderRadius;
    }

    MouseArea{
        id: mouseArea

        anchors.fill: parent
        hoverEnabled: true

        onClicked: {
            if(button.unit === "lux"){
                button.unit = "fc";
            }else if(button.unit === "fc"){
                button.unit = "lux";
            }
        }

        onPressed: button.pressed();
        onReleased: button.released();
    }
}

