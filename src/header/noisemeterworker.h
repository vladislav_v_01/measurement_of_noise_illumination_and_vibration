#pragma once

#include <QAudioDeviceInfo>
#include <QAudioInput>
#include <QLineSeries>
#include <QObject>
#include <QQuickItem>

#include <memory>

#include "noisemeter.h"
#include "property.h"
#include "qcustomplotqml.h"

class NoiseMeterWorker : public QObject
{
    Q_OBJECT
    PROPERTY(QObject*, noiseChartView);
    PROPERTY(QQuickItem*, minValueLabel);
    PROPERTY(QQuickItem*, avgValueLabel);
    PROPERTY(QQuickItem*, maxValueLabel);
    PROPERTY(QQuickItem*, vibrometerArrowImage);
    PROPERTY(QtCharts::QAbstractSeries*, series);
    Q_PROPERTY(QtCharts::QAbstractSeries* series READ get_series WRITE
                   set_series FINAL);
    PROPERTY(double, angle);
    Q_PROPERTY(double angle READ get_angle WRITE
                   set_angle /*NOTIFY angleChanged*/ FINAL);
    //    PROPERTY(qreal,        summOfMeasurings);
    PROPERTY(unsigned int, countOfMeasurings);
    qreal m_summOfMeasurings = 0;
    //    unsigned int, countOfMeasurings = 0;
    const uint8_t m_countOFSymbolsOnValueLabels = 3;

public:
    NoiseMeterWorker(QObject* p_parent = nullptr);
    ~NoiseMeterWorker();
Q_SIGNALS:
    void angleChanged();
public Q_SLOTS:
    Q_INVOKABLE void onItemCompleted(QQuickItem* newItem);
    Q_INVOKABLE void onItemDestroyed(QQuickItem* newItem);
    Q_INVOKABLE void onNewSeries();
    Q_INVOKABLE void onNewDataWrited(QVector<QPointF> data);

private:
    void initializeAudio(const QAudioDeviceInfo& deviceInfo);

private:
    NoiseMeter*  m_audioInfo;
    QAudioInput* m_audioInput;
};
