#pragma once

#include <QAudioBuffer>
#include <QAudioFormat>
#include <QIODevice>
//#include <QXYSeries>

#include <memory>

#include "property.h"
#include "qcustomplot.h"

class NoiseMeter : public QIODevice
{
    Q_OBJECT
public:
    NoiseMeter(QObject* p_parent = nullptr);
    ~NoiseMeter();
    static const int sampleCount = 2000;
Q_SIGNALS:
    void newDataWrited(QVector<QPointF> data);
public Q_SLOTS:
    qint64 readData(char* data, qint64 maxlen) override;
    qint64 writeData(const char* data, qint64 len) override;

private:
    QVector<QPointF> m_buffer;
};
Q_DECLARE_METATYPE(QVector<QPointF>);
