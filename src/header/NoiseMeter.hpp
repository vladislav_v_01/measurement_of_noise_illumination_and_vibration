//#pragma once

//#include <iostream>

//#include <QObject>

//#include <QDebug>
//#include <QTimer>

//#include <QtEndian>

//#include <QAudioInput>
//#include <QIODevice>

//#include <QMainWindow>
//#include <QPainter>

//#include <QComboBox>
//#include <QPushButton>
//#include <QSlider>
//#include <QVBoxLayout>
//#include <QWidget>

//#include "qcustomplot.h"
//#include "staticaldataprocessor.h"

//#define NEED_DEBUG_INFO

// class RenderArea;
// class AudioInfo;
// class NoiseMeter;
///*
// class AudioInfo : public QIODevice
//{
//    Q_OBJECT
// public:
//    AudioInfo(const QAudioFormat& format);

// public slots:

//    void start();
//    void stop();

//    qreal level() const { return m_level; }

//    qint64 readData(char* data, qint64 maxlen) override;
//    qint64 writeData(const char* data, qint64 len) override;

//    std::vector<quint32> getSampledAudio();
//    std::vector<qreal>   getSampledAudioIndB();
//    std::vector<float>   getSampledAudioF();
//    std::vector<float>   getSampledBufferF();

// signals:
//    void bufferSampled();
//    void audioSampled();

// private:
//    const QAudioFormat m_format;
//    quint32            m_maxAmplitude = 0;
//    qreal              m_level        = 0.0; // 0.0 <= m_level <= 1.0

//    std::vector<quint32> m_sampledAudio;
//    std::vector<qreal>   m_sampledAudioInbB;
//    std::vector<float>   m_sampledAudioF;
//    std::vector<float>   m_sampledBufferF;

//    bool mesauringFinished = false;
//#ifdef NEED_DEBUG_INFO
//    quint32 m_maxAudioLevel;
//#endif

// signals:
//    void update();
//};
//*/
// class RenderArea : public QWidget
//{
//    Q_OBJECT

// public:
//    explicit RenderArea(QWidget* parent = nullptr);

//    void setLevel(qreal value);

// protected:
//    void paintEvent(QPaintEvent* event) override;

// private:
//    qreal   m_level = 0;
//    QPixmap m_pixmap;
//};

// class NoiseMeter : public QMainWindow
//{
//    Q_OBJECT

// public:
//    NoiseMeter();
//    ~NoiseMeter();

// private:
//    void initializeWindow();
//    void initializeAudio(const QAudioDeviceInfo& deviceInfo);

// private slots:
//    void toggleMode();
//    void toggleSuspend();
//    void deviceChanged(int index);
//    void sliderChanged(int value);

//    void doStaticalBufferDataProcessing();
//    void doStaticalAudioDataProcessing();

// private:
//    bool               buildBlackmanHarrisWindow(std::vector<float>& window);
//    std::vector<float> buildCoreLPFilt(double fc, double BW);
//    std::vector<float> buildCoreHPFilt(double fc, double BW);
//    std::vector<float> convolve(const std::vector<float>& u,
//                                const std::vector<float>& v);
//    void               scaleAxisToEachOther();

// private:
//    // Owned by layout
//    RenderArea*  m_canvas              = nullptr;
//    QPushButton* m_modeButton          = nullptr;
//    QPushButton* m_suspendResumeButton = nullptr;
//    QComboBox*   m_deviceBox           = nullptr;
//    QSlider*     m_volumeSlider        = nullptr;

//    QScopedPointer<AudioInfo>   m_audioInfo;
//    QScopedPointer<QAudioInput> m_audioInput;
//    bool                        m_pullMode = false;

//    StaticalDataProcessor dataHandler;

//    QCustomPlot* freaqunceCustomPlot;

//    std::vector<float> LPFilt;
//    std::vector<float> HPFilt;
//    std::vector<float> BHWindow;

//    std::vector<float> signalOfLastSecond;
//    std::vector<float> signalOfPartOfSecond;
//};
