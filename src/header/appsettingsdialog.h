#ifndef ADCSETTINGSDIALOG_H
#define ADCSETTINGSDIALOG_H

#include <QDialog>

namespace Ui
{
class AppSettingsDialog;
}

class AppSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AppSettingsDialog(QWidget* parent = nullptr);
    ~AppSettingsDialog();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::AppSettingsDialog* ui;
};

#endif // ADCSETTINGSDIALOG_H
