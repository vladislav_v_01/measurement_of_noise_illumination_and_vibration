#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <QAudioFormat>
#include <QObject>

#include "property.h"

class AppSettings : QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE AppSettings(QObject* parent = nullptr);
    Q_INVOKABLE ~AppSettings();

    PROPERTY_DEFAULT(int, sampleRate, 48000);
    PROPERTY_DEFAULT(int, channelCount, 1);
    PROPERTY_DEFAULT(int, sampleSize, 32);
    PROPERTY_DEFAULT(QAudioFormat::SampleType, sampleType, QAudioFormat::Float);
    PROPERTY_DEFAULT(int, bufferSize, 2048);
};

#endif // APPSETTINGS_H
