#pragma once

#include <memory>

#include "luxmeter.h"

#include "qcustomplotqml.h"

// TODO: 1) рефакторинг;
//       2) реализация логики кнопок.

class LuxMeterWorker : public QObject
{
    Q_OBJECT

    PROPERTY(LuxMeter*, luxMeter);
    PROPERTY(CustomPlotItem*, customPlot);
    PROPERTY(QQuickItem*, currValueLabel);
    PROPERTY(QQuickItem*, currUnitLabel);
    PROPERTY(QQuickItem*, minValueLabel);
    PROPERTY(QQuickItem*, avgValueLabel);
    PROPERTY(QQuickItem*, maxValueLabel);
    PROPERTY(QQuickItem*, rippleFactorLabel);
    PROPERTY(QObject*, startStopButton);
    PROPERTY(QObject*, changeUnitsButton);
    PROPERTY(qreal, summOfMeasurings);
    PROPERTY(unsigned int, countOfMeasurings);
    PROPERTY(float, maxValueForRF);
    PROPERTY(float, minValueForRF);
    PROPERTY(float, summValuesForRF);
    PROPERTY(unsigned int, countOfMeasuringsRF);
    PROPERTY(QTimer*, rippleFactorTimer);
    PROPERTY_DEFAULT(double, currYAxisRangeUpper, 250);
    const uint8_t m_countOFSymbolsOnValueLabels = 5;

public:
    Q_INVOKABLE LuxMeterWorker(QObject* p_parent = nullptr);

    ~LuxMeterWorker();

public Q_SLOTS:
    Q_INVOKABLE void onStopStartButtonClicked();

    Q_INVOKABLE void onResetButtonClicked();

    Q_INVOKABLE void onIncreaseButtonClicked();

    Q_INVOKABLE void onReduceButtonClicked();

    Q_INVOKABLE void onUnitButtonClicked();

    Q_INVOKABLE void onItemCompleted(QQuickItem* newItem,
                                     const char* newParameter = "luxmeterPlot");

    Q_INVOKABLE void onItemDestroyed(QQuickItem* newItem);

    Q_INVOKABLE void onNewValue(qreal newLuxValue);

    Q_INVOKABLE void onNewRFMeasuringTime(QString newMeasuringTime);

    Q_INVOKABLE void onRFTimerTimeout();
};
