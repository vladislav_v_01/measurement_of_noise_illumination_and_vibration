#ifndef AUDIOINFO_H
#define AUDIOINFO_H

#include <QAudioFormat>
#include <QIODevice>

class AudioInfo : public QIODevice
{
    Q_OBJECT
public:
    AudioInfo(const QAudioFormat& format);
    ~AudioInfo();

public Q_SLOTS:

    void start();
    void stop();

    qint64 readData(char* data, qint64 maxlen) override;
    qint64 writeData(const char* data, qint64 len) override;

Q_SIGNALS:
    void bufferSampled(std::vector<float> sampledBuffer);

private:
    void doStaticalProcessing(std::vector<float> sampledBuffer);

private:
    const QAudioFormat m_format;
    quint32            m_maxAmplitude = 0;

    std::vector<float> m_sampledBuffer;
};

#endif // AUDIOINFO_H
