#pragma once

#include <QLightSensor>
#include <QObject>

#include "property.h"

/**
 * @brief The LuxMeter class is for getting values from light sensor of
 * smartphone.
 */
class LuxMeter : public QObject
{
    Q_OBJECT

    PROPERTY(QLightSensor*, lightSensor);

    PROPERTY(qreal, lux);

public:
    Q_INVOKABLE explicit LuxMeter(QObject* parent = nullptr);

    ~LuxMeter();

public Q_SLOTS:
    /**
     * @brief getLux returns last read value in lux.
     */
    Q_INVOKABLE qreal getLux() const;

    /**
     * @brief startListening launches reading data from light sensor.
     */
    Q_INVOKABLE void startListening();

    /**
     * @brief stopListening stops reading data from light sensor.
     */

    Q_INVOKABLE void stopListening();
Q_SIGNALS:
    /**
     * @brief luxChanged emits when new data arrived from light sensor.
     * @param newLuxValue is a new value in lux from light sensor.
     */
    void newValue(qreal newLuxValue);
};
