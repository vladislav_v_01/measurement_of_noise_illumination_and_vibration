#ifndef MICROPHONE_H
#define MICROPHONE_H

#include <QAudioDeviceInfo>
#include <QAudioInput>
#include <QObject>

#include <memory>

#include "audioinfo.h"

#include "qcustomplot.h"

// std::mutex g_mutex;

class Microphone : public QIODevice
{
    Q_OBJECT
public:
    Microphone(QObject* parent = nullptr);
    ~Microphone();

private:
    void initializeAudio(const QAudioDeviceInfo& deviceInfo);

public Q_SLOTS:
    void start();
    void stop();

    void toggleMode();
    void toggleSuspend();

private Q_SLOTS:
    //    void bufferTransfer(std::vector<float> sampledBuffer);

    qint64 readData(char* data, qint64 maxlen) override;
    qint64 writeData(const char* data, qint64 len) override;

    void doStaticalDataProcessing(std::vector<float> sampledBuffer);
    void doStaticalDataProcessingForTwoSeconds(
        std::vector<float> sampledBuffer);

private:
    void initAudioInfo();
    bool buildBlackmanHarrisWindow(std::vector<float>& window);

Q_SIGNALS:
    //    void bufferSampled(std::vector<float> sampledBuffer);
    void drawFrequencyDomain(QVector<QCPGraphData> dataInFrequencyDomain);
    void drawFrequencyDomainForTwoSeconds(
        QVector<QCPGraphData> dataInFrequencyDomain);
    void drawTimeDomain(QVector<QCPGraphData> dataInTimeDomain);

private:
    QAudioFormat m_format;
    quint32      m_maxAmplitude = 0;

    //    std::unique_ptr<AudioInfo>   m_audioInfo;
    std::unique_ptr<QAudioInput> m_audioInput;
    bool                         m_pullMode = false;

    // partOfValuesForLastSecond, valuesForLastSecond, dataInFrequencyDomain,
    // dataInTimeDomain используются только в функции doStaticalDataProcessing()
    // если нужно буде использовать где-то ещё нужно добавить std::mutex!!!
    QVector<QCPGraphData> dataInFrequencyDomain;
    QVector<QCPGraphData> dataInTimeDomain;
    std::vector<float>    partOfValuesForLastSecond;
    std::vector<float>    valuesForLastSecond;
    // BlackmanHarrisWidnow инициализируется в конструкторе, однако, нужно
    // привязать переинициализацию к сигналу о том, что значение
    // изменилось!!!!(добавить std::mutex)
    std::vector<float> BlackmanHarrisWidnow;

    std::vector<float> valuesForTwoSeconds;
};

#endif // MICROPHONE_H
