#ifndef GLOBALWORKER_H
#define GLOBALWORKER_H

#include <QObject>

#include <memory>

#include "luxmeterworker.h"
#include "noisemeterworker.h"

#include "property.h"

class GlobalWorker : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE GlobalWorker()
        : luxMeterWorker{ std::make_shared<LuxMeterWorker>(this) }
        , noiseMeterWorker{ std::make_shared<NoiseMeterWorker>(this) }
    {
    }
    Q_INVOKABLE ~GlobalWorker() Q_DECL_OVERRIDE {}

public:
    auto& getLuxMeterWorker() const { return this->luxMeterWorker; }

    auto& getNoiseMeterWorker() const { return this->noiseMeterWorker; }

private:
    void setLuxMeterWorker(std::shared_ptr<LuxMeterWorker>& luxMeterWorker)
    {
        if (luxMeterWorker != nullptr)
        {
            this->luxMeterWorker = luxMeterWorker;
        }
    }

    void setNoiseMeterWorker(
        std::shared_ptr<NoiseMeterWorker>& noiseMeterWorker)
    {
        if (noiseMeterWorker != nullptr)
        {
            this->noiseMeterWorker = noiseMeterWorker;
        }
    }

private:
    std::shared_ptr<LuxMeterWorker>   luxMeterWorker;
    std::shared_ptr<NoiseMeterWorker> noiseMeterWorker;
};

#endif // GLOBALWORKER_H
