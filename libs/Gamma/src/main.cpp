
#include <QApplication>
#include <QDebug>

#include "Gamma/FFT.h"
#include "Gamma/Print.h"
#include "Gamma/Types.h"
using namespace gam;

int main(int argc, char* argv[])
{
    qDebug() << "[INFO] Init data" << Qt::endl;

    const int   N = 32; // Transform size
    float       sig[N]; // Time-/position-domain signal
    float       buf[N]; // Transform buffer
    RFFT<float> fft(N); // Real-to-complex FFT

    qDebug() << "[INFO] Create signal" << Qt::endl;

    // Create signal
    for (int i = 0; i < N; ++i)
    {
        float p = float(i) / N * M_2PI;
        sig[i]  = 1 + cos(p) + cos(2 * p) + sin(3 * p);
        sig[i] += i & 1 ? -1 : 1;
    }

    qDebug() << "[INFO] Use CFFT" << Qt::endl;

    // CFFT operates in-place, so we copy our input signal to another buffer
    for (int i = 0; i < N; ++i)
        buf[i] = sig[i];

    qDebug() << "[INFO] Perform real-to-complex" << Qt::endl;

    // Perform real-to-complex forward transform (time/position to frequency)
    fft.forward(buf);

    qDebug() << "[INFO] Print frequency-domain samples" << Qt::endl;

    // Print out frequency-domain samples
    int numBins = N / 2 + 1;

    for (int i = 0; i < numBins; ++i)
    {
        Complex<float> c;
        if (0 == i)
            c(buf[0], 0);
        else if (N / 2 == i)
            c(buf[N - 1], 0);
        else
            c(buf[i * 2 - 1], buf[i * 2]);

        qDebug("[%2d] ", i);
        qDebug("% 5.2f % 5.2f ", c[0], c[1]);
        qDebug("%s", plotString(c[0], 32, true, true, "o").c_str());
        qDebug("%s", plotString(c[1], 32, true, true, "o").c_str());
        qDebug("\n");
    }

    // Perform complex-to-real inverse transform (frequency to time/position)
    fft.inverse(buf);

    // Print out original signal versus forward/inverse transformed signal
    qDebug("\n");
    for (int i = 0; i < N; ++i)
    {
        qDebug("[%2d] ", i);
        qDebug("% 5.2f % 5.2f ", sig[i], buf[i]);
        qDebug("%s", plotString(sig[i] / 5, 32, true, true, "o").c_str());
        qDebug("%s", plotString(buf[i] / 5, 32, true, true, "o").c_str());
        qDebug("\n");
    }

    return 0;
}
