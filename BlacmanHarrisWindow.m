## Copyright (C) 2021
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {@var{retval} =} Blacman-HarrisWindow (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author:  <praxisan@ArchLinux>
## Created: 2021-04-25

function retval = BlacmanHarrisWindow (n)

 a0 = 0.35875;
 a1 = 0.48829;
 a2 = 0.14128;
 a3 = 0.01168;
 M_PI = pi;

 idx = 0;
 retval = [];
 while( idx < n )
    valueOfWindow = a0 - (a1 * cos( (2.0 * M_PI * idx) / (n - 1) )) + (a2 * cos( (4.0 * M_PI * idx) / (n - 1) )) - (a3 * cos( (6.0 * M_PI * idx) / (n - 1) ));
    retval = [retval valueOfWindow];
    idx++;
 endwhile
endfunction
